"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RainbirdProvider = exports.RainbirdContext = void 0;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// Testing context creation is hard and this function is simple

/* istanbul ignore next */
var RainbirdContext = /*#__PURE__*/(0, _react.createContext)({
  apiKey: "",
  baseURL: "",
  kmID: "",
  sessionID: ""
});
exports.RainbirdContext = RainbirdContext;

var RainbirdProvider = function RainbirdProvider(_ref) {
  var apiKey = _ref.apiKey,
      baseURL = _ref.baseURL,
      children = _ref.children,
      kmID = _ref.kmID,
      kmVersionID = _ref.kmVersionID,
      options = _ref.options,
      sessionID = _ref.sessionID;

  var _useState = (0, _react.useState)(apiKey),
      _useState2 = _slicedToArray(_useState, 2),
      rbApiKey = _useState2[0],
      setRbApiKey = _useState2[1];

  var _useState3 = (0, _react.useState)(baseURL),
      _useState4 = _slicedToArray(_useState3, 2),
      rbBaseURL = _useState4[0],
      setRbBaseURL = _useState4[1];

  var _useState5 = (0, _react.useState)(kmID),
      _useState6 = _slicedToArray(_useState5, 2),
      rbKmID = _useState6[0],
      setRbKmID = _useState6[1];

  var _useState7 = (0, _react.useState)(kmVersionID),
      _useState8 = _slicedToArray(_useState7, 2),
      kmVersion = _useState8[0],
      setKmVersion = _useState8[1];

  var _useState9 = (0, _react.useState)(sessionID),
      _useState10 = _slicedToArray(_useState9, 2),
      internalSessionID = _useState10[0],
      setSessionID = _useState10[1];

  (0, _react.useEffect)(function () {
    setRbApiKey(apiKey);
    setRbBaseURL(baseURL);
    setRbKmID(kmID);
    setKmVersion(kmVersionID);
    setSessionID(sessionID);
  }, [apiKey, baseURL, kmID, kmVersionID, sessionID]);
  return /*#__PURE__*/_react.default.createElement(RainbirdContext.Provider, {
    value: {
      apiKey: rbApiKey,
      baseURL: rbBaseURL,
      kmID: rbKmID,
      kmVersion: kmVersion,
      options: options,
      sessionID: internalSessionID,
      setKmVersion: setKmVersion,
      setSessionID: setSessionID
    }
  }, children);
};

exports.RainbirdProvider = RainbirdProvider;