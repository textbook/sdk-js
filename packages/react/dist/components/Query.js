"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Query = void 0;

var _hooks = require("../hooks");

var Query = function Query(_ref) {
  var subject = _ref.subject,
      relationship = _ref.relationship,
      object = _ref.object,
      children = _ref.children;
  var data = (0, _hooks.useQuery)(subject, relationship, object);
  return children ? children(data) : null;
};

exports.Query = Query;