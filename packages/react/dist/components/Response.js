"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Response = void 0;

var _hooks = require("../hooks");

var Response = function Response(_ref) {
  var children = _ref.children;
  var request = (0, _hooks.useResponse)();
  return children ? children({
    request: request
  }) : null;
};

exports.Response = Response;