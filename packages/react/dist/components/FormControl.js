"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormControl = void 0;

var _react = _interopRequireDefault(require("react"));

var _helpers = require("../helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FormControl = function FormControl(_ref) {
  var data = _ref.data,
      multiString = _ref.multiString,
      singleString = _ref.singleString,
      multiStringAdd = _ref.multiStringAdd,
      singleStringAdd = _ref.singleStringAdd,
      singleNumber = _ref.singleNumber,
      singleDate = _ref.singleDate,
      multiDate = _ref.multiDate,
      singleTruth = _ref.singleTruth,
      certaintySelect = _ref.certaintySelect,
      multiNumber = _ref.multiNumber,
      multiNumberAdd = _ref.multiNumberAdd,
      fallback = _ref.fallback;
  var formInputs = (0, _helpers.getFormInputType)(data);
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, formInputs.map(function (input) {
    switch (input.control) {
      case "single-string-canAdd":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleStringAdd), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "single-string":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleString), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "multi-string-canAdd":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, multiStringAdd), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "multi-string":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, multiString), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "single-date-canAdd":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleDate), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "single-number-canAdd":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleNumber), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "single-truth-canAdd":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleTruth), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "single-date":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleDate), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "multi-date":
      case "multi-date-canAdd":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, multiDate), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "single-number":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleNumber), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "multi-number-canAdd":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, multiNumberAdd), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "multi-number":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, multiNumber), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      case "single-truth":
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, singleTruth), /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, input.certainty && certaintySelect));

      default:
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, {
          key: input.control
        }, fallback);
    }
  }));
};

exports.FormControl = FormControl;