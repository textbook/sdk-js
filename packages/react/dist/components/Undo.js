"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Undo = void 0;

var _hooks = require("../hooks");

var Undo = function Undo(_ref) {
  var children = _ref.children;
  var request = (0, _hooks.useUndo)();
  return children ? children({
    request: request
  }) : null;
};

exports.Undo = Undo;