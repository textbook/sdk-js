"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Start = void 0;

var _hooks = require("../hooks");

var Start = function Start(_ref) {
  var children = _ref.children;
  var data = (0, _hooks.useStart)();
  return children ? children(data) : null;
};

exports.Start = Start;