"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Rainbird = void 0;

var _react = _interopRequireDefault(require("react"));

var _Query = require("./Query");

var _Start = require("./Start");

var _context = require("../context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Rainbird = function Rainbird(_ref) {
  var onError = _ref.onError,
      onLoad = _ref.onLoad,
      baseURL = _ref.baseURL,
      kmID = _ref.kmID,
      apiKey = _ref.apiKey,
      options = _ref.options,
      kmVersionID = _ref.kmVersionID,
      sessionID = _ref.sessionID,
      subject = _ref.subject,
      relationship = _ref.relationship,
      object = _ref.object,
      children = _ref.children;
  return /*#__PURE__*/_react.default.createElement(_context.RainbirdProvider, {
    baseURL: baseURL,
    kmID: kmID,
    apiKey: apiKey,
    options: options,
    kmVersionID: kmVersionID,
    sessionID: sessionID
  }, /*#__PURE__*/_react.default.createElement(_Start.Start, {
    onLoad: onLoad,
    onError: onError
  }, function (start) {
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, start.loading && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, onLoad), start.error && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, onError(start.error)), start.data && /*#__PURE__*/_react.default.createElement(_context.InteractionProvider, null, function (_ref2) {
      var data = _ref2.data,
          loading = _ref2.loading,
          error = _ref2.error;
      return /*#__PURE__*/_react.default.createElement(_Query.Query, {
        subject: subject,
        relationship: relationship,
        object: object,
        onLoad: onLoad,
        onError: onError
      }, function () {
        return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, loading && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, onLoad), error && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, onError(error)), data && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, children ? children(data) : null));
      });
    }));
  }));
};

exports.Rainbird = Rainbird;