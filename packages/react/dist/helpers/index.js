"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "getFormInputType", {
  enumerable: true,
  get: function get() {
    return _getFormInputType.getFormInputType;
  }
});

var _getFormInputType = require("./getFormInputType");