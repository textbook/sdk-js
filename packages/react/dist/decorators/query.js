"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withQuery = void 0;

var _react = _interopRequireDefault(require("react"));

var _hooks = require("../hooks");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var withQuery = function withQuery(WrappedComponent) {
  return function (_ref) {
    var subject = _ref.subject,
        object = _ref.object,
        relationship = _ref.relationship,
        props = _objectWithoutProperties(_ref, ["subject", "object", "relationship"]);

    var query = (0, _hooks.useQuery)(subject, relationship, object);
    return /*#__PURE__*/_react.default.createElement(WrappedComponent, _extends({}, props, {
      query: query
    }));
  };
};

exports.withQuery = withQuery;