"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withUndo = void 0;

var _react = _interopRequireDefault(require("react"));

var _hooks = require("../hooks");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var withUndo = function withUndo(WrappedComponent) {
  return function (props) {
    var undo = (0, _hooks.useUndo)();
    return /*#__PURE__*/_react.default.createElement(WrappedComponent, _extends({}, props, {
      undo: undo
    }));
  };
};

exports.withUndo = withUndo;