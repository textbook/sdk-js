"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useResponse = void 0;

var _react = require("react");

var _sdk = require("@rainbird/sdk");

var _context2 = require("../context");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var useResponse = function useResponse() {
  var _useContext = (0, _react.useContext)(_context2.RainbirdContext),
      baseURL = _useContext.baseURL,
      options = _useContext.options,
      sessionID = _useContext.sessionID;

  var _useContext2 = (0, _react.useContext)(_context2.InteractionContext),
      setInteraction = _useContext2.setInteraction;

  var sendResponse = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(answers) {
      var body;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              setInteraction({
                loading: true
              });
              _context.next = 4;
              return (0, _sdk.response)(baseURL, sessionID, answers, options);

            case 4:
              body = _context.sent;
              setInteraction({
                payload: body
              });
              _context.next = 11;
              break;

            case 8:
              _context.prev = 8;
              _context.t0 = _context["catch"](0);
              setInteraction({
                error: _context.t0
              });

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 8]]);
    }));

    return function sendResponse(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  return sendResponse;
};

exports.useResponse = useResponse;