"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useInteraction = void 0;

var _react = require("react");

var _context = require("../context");

var useInteraction = function useInteraction() {
  var context = (0, _react.useContext)(_context.InteractionContext);
  return context;
};

exports.useInteraction = useInteraction;