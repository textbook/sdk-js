import { getFormInputType } from "../getFormInputType";

describe("getFormInputType", () => {
  it("Returns an empty array if no data is provided", () => {
    expect(getFormInputType()).toEqual([]);
  });

  describe("Returns the correct input types for different data shapes", () => {
    const baseData = {
      allowCF: true,
      allowUnknown: false,
      canAdd: false,
      concepts: [],
      dataType: "string",
      knownAnswers: [],
      plural: false,
      prompt: "Which country is the lives in of ellie?",
      relationship: "lives in",
      subject: "ellie",
      type: "Second Form Object",
    };

    it("can build up single-string", () => {
      const result = [{ control: "single-string", certainty: true }];
      expect(getFormInputType(baseData)).toEqual(result);
    });

    it("can build up single-string-canAdd", () => {
      const data = { ...baseData, canAdd: true };
      const result = [{ control: "single-string-canAdd", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up single-date", () => {
      const data = { ...baseData, dataType: "date" };
      const result = [{ control: "single-date", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up multi-date", () => {
      const data = { ...baseData, dataType: "date", plural: true };
      const result = [{ control: "multi-date", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up multi-date-canAdd", () => {
      const data = {
        ...baseData,
        dataType: "date",
        plural: true,
        canAdd: true,
      };
      const result = [{ control: "multi-date-canAdd", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up single-truth", () => {
      const data = { ...baseData, dataType: "truth" };
      const result = [{ control: "single-truth", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up single-number", () => {
      const data = { ...baseData, dataType: "number" };
      const result = [{ control: "single-number", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up multi-string", () => {
      const data = { ...baseData, dataType: "string", plural: true };
      const result = [{ control: "multi-string", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up multi-string with no cf", () => {
      const newData = { allowCF: false, dataType: "string", plural: true };
      const data = { ...baseData, ...newData };
      const result = [{ control: "multi-string", certainty: false }];
      expect(getFormInputType(data)).toEqual(result);
    });

    it("can build up single truth when first form", () => {
      const data = { ...baseData, dataType: undefined, type: "First Form" };
      const result = [{ control: "single-truth", certainty: true }];
      expect(getFormInputType(data)).toEqual(result);
    });
  });
});
