import { useContext, useReducer } from "react";
import { evidence } from "@rainbird/sdk";
import { RainbirdContext } from "../context";

const reducer = (_, action) => {
  if (action.error) return { loading: false, error: action.error, data: false };
  return { loading: false, error: false, data: action.payload };
};

export const useEvidence = () => {
  const { baseURL, options, sessionID } = useContext(RainbirdContext);
  const [state, dispatch] = useReducer(reducer, {
    loading: true,
    error: false,
    data: null,
  });

  const getEvidence = async (factID, evidenceKey) => {
    const o = options || {};
    if (evidenceKey !== undefined) o.evidenceKey = evidenceKey;

    try {
      const body = await evidence(baseURL, sessionID, factID, o);
      dispatch({ payload: body });
    } catch (e) {
      dispatch({ error: e });
    }
  };

  return [state, getEvidence];
};
