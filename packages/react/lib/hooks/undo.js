import { useContext } from "react";
import { undo } from "@rainbird/sdk";
import { RainbirdContext, InteractionContext } from "../context";

export const useUndo = () => {
  const { baseURL, options, sessionID } = useContext(RainbirdContext);
  const { setInteraction } = useContext(InteractionContext);

  const undoResponse = async () => {
    try {
      setInteraction({ loading: true });
      const body = await undo(baseURL, sessionID, {}, options);
      setInteraction({ payload: body });
    } catch (e) {
      setInteraction({ error: e });
    }
  };

  return undoResponse;
};
