import { useContext } from "react";
import { response } from "@rainbird/sdk";
import { RainbirdContext, InteractionContext } from "../context";

export const useResponse = () => {
  const { baseURL, options, sessionID } = useContext(RainbirdContext);
  const { setInteraction } = useContext(InteractionContext);

  const sendResponse = async (answers) => {
    try {
      setInteraction({ loading: true });
      const body = await response(baseURL, sessionID, answers, options);
      setInteraction({ payload: body });
    } catch (e) {
      setInteraction({ error: e });
    }
  };

  return sendResponse;
};
