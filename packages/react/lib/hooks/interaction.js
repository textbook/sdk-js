import { useContext } from "react";
import { InteractionContext } from "../context";

export const useInteraction = () => {
  const context = useContext(InteractionContext);

  return context;
};
