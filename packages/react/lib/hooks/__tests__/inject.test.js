import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { useInject } from "..";

jest.mock("@rainbird/sdk");

describe("useInject", () => {
  const data = [
    {
      cf: "99",
      object: "England",
      relationship: "lives in",
      subject: "David",
    },
  ];

  afterEach(() => jest.clearAllMocks());

  it("calls base SDK inject", async () => {
    const Inject = () => {
      const inject = useInject()[1];
      return (
        <button type="button" data-testid="inject" onClick={() => inject(data)}>
          Inject
        </button>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionId"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Inject />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("inject");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.inject).toHaveBeenCalledTimes(1);
      expect(base.inject).toHaveBeenCalledWith(
        "/rainbird",
        "sessionId",
        [
          {
            cf: "99",
            object: "England",
            relationship: "lives in",
            subject: "David",
          },
        ],
        undefined,
      );
    });
  });

  it("provides options to inject", async () => {
    const Inject = () => {
      const inject = useInject()[1];
      return (
        <button type="button" data-testid="inject" onClick={() => inject(data)}>
          Inject
        </button>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionId"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        options={{ engine: "v2.0" }}
      >
        <Inject />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("inject");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.inject).toHaveBeenCalledTimes(1);
      expect(base.inject).toHaveBeenCalledWith(
        "/rainbird",
        "sessionId",
        [
          {
            cf: "99",
            object: "England",
            relationship: "lives in",
            subject: "David",
          },
        ],
        { engine: "v2.0" },
      );
    });
  });

  it("returns loading state", async () => {
    const Inject = () => {
      const [response, inject] = useInject();
      return (
        <div>
          <button
            type="button"
            data-testid="inject"
            onClick={() => inject(data)}
          >
            Inject
          </button>
          {response.loading && <p data-testid="loading">loading</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionId"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Inject />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("inject");
    fireEvent.click(button);
    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("returns error state", async () => {
    base.inject = jest.fn(async () => {
      throw new Error();
    });

    const Inject = () => {
      const [response, inject] = useInject();
      return (
        <div>
          <button
            type="button"
            data-testid="inject"
            onClick={() => inject(data)}
          >
            Inject
          </button>
          {response.error && <p data-testid="error">error</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionId"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Inject />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("inject");
    fireEvent.click(button);
    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("returns success state", async () => {
    base.inject = jest.fn(async () => {
      return { data: "thisIsTheData" };
    });

    const Inject = () => {
      const [response, inject] = useInject();
      return (
        <div>
          <button
            type="button"
            data-testid="inject"
            onClick={() => inject(data)}
          >
            Inject
          </button>
          {response.data && <p data-testid="success">success</p>}
        </div>
      );
    };

    render(
      <RainbirdProvider
        sessionID="sessionId"
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
      >
        <Inject />
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("inject");
    fireEvent.click(button);
    await waitFor(() => {
      const success = screen.getByTestId("success");
      expect(success.textContent).toEqual("success");
    });
  });
});
