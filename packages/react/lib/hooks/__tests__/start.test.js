import React, { useContext } from "react";
import { render, waitFor, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdContext, RainbirdProvider } from "../../context";
import { useStart } from "../start";

jest.mock("@rainbird/sdk");

describe("useStart", () => {
  it("calls base SDK start and stores response", async () => {
    base.start = jest.fn(async () => ({
      sessionID: "thisIsTheSessionID",
      kmVersionID: "thisIsTheKmVersionID",
    }));

    let retSessionID;
    let retKmVersion;

    const Child = () => {
      const { kmVersion, sessionID } = useContext(RainbirdContext);
      useStart();
      retSessionID = sessionID;
      retKmVersion = kmVersion;
      return <div />;
    };

    render(
      <RainbirdProvider kmID="1234" apiKey="myApiKey" baseURL="/rainbird">
        <Child />
      </RainbirdProvider>,
    );

    await waitFor(() => {
      expect(base.start).toHaveBeenCalledTimes(1);
      expect(base.start).toHaveBeenCalledWith(
        "/rainbird",
        "myApiKey",
        "1234",
        undefined,
      );
      expect(retSessionID).toEqual("thisIsTheSessionID");
      expect(retKmVersion).toEqual("thisIsTheKmVersionID");
    });
  });

  it("should pass through context-scope options", async () => {
    base.start = jest.fn(async () => ({
      sessionID: "thisIsTheSessionID",
      kmVersionID: "thisIsTheKmVersionID",
    }));

    let retSessionID;
    let retKmVersion;

    const Child = () => {
      const { kmVersion, sessionID } = useContext(RainbirdContext);
      useStart();
      retSessionID = sessionID;
      retKmVersion = kmVersion;
      return <div />;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        options={{ engine: "Experimental (Beta)" }}
      >
        <Child />
      </RainbirdProvider>,
    );

    await waitFor(() => {
      expect(base.start).toHaveBeenCalledTimes(1);
      expect(base.start).toHaveBeenCalledWith("/rainbird", "myApiKey", "1234", {
        engine: "Experimental (Beta)",
      });
      expect(retSessionID).toEqual("thisIsTheSessionID");
      expect(retKmVersion).toEqual("thisIsTheKmVersionID");
    });
  });

  it("Returns loading state", async () => {
    base.start = jest.fn(async () => ({
      sessionID: "thisIsTheSessionID",
      kmVersionID: "thisIsTheKmVersionID",
    }));

    const Child = () => {
      const data = useStart();

      return <>{data.loading && <p data-testid="loading">loading</p>}</>;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        options={{ engine: "Experimental (Beta)" }}
      >
        <Child />
      </RainbirdProvider>,
    );

    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("Returns error state", async () => {
    base.start = jest.fn(async () => {
      throw new Error();
    });

    const Child = () => {
      const data = useStart();

      return <>{data.error && <p data-testid="error">error</p>}</>;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        options={{ engine: "Experimental (Beta)" }}
      >
        <Child />
      </RainbirdProvider>,
    );

    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("Returns success state", async () => {
    base.start = jest.fn(async () => {
      return { data: "hello" };
    });

    const Child = () => {
      const data = useStart();

      return <>{data.data && <p data-testid="success">success</p>}</>;
    };

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        options={{ engine: "Experimental (Beta)" }}
      >
        <Child />
      </RainbirdProvider>,
    );

    await waitFor(() => {
      const success = screen.getByTestId("success");
      expect(success.textContent).toEqual("success");
    });
  });
});
