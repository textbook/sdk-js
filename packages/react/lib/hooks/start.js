import { useContext, useReducer, useEffect } from "react";
import { start } from "@rainbird/sdk";
import { RainbirdContext } from "../context";

const reducer = (_, action) => {
  if (action.error) return { loading: false, error: action.error, data: false };
  return { loading: false, error: false, data: action.payload };
};

export const useStart = () => {
  const {
    baseURL,
    apiKey,
    kmID,
    options,
    setSessionID,
    setKmVersion,
  } = useContext(RainbirdContext);
  const [state, dispatch] = useReducer(reducer, {
    loading: true,
    error: false,
    data: null,
  });

  useEffect(() => {
    async function startSession() {
      try {
        const body = await start(baseURL, apiKey, kmID, options);
        setSessionID(body.sessionID);
        setKmVersion(body.kmVersionID);
        dispatch({ payload: body });
      } catch (e) {
        dispatch({ error: e });
      }
    }
    startSession();
  }, [baseURL, apiKey, kmID]);

  return state;
};
