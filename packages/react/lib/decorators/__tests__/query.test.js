import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider, InteractionProvider } from "../../context";
import { withQuery } from "../query";

jest.mock("@rainbird/sdk");

describe("withQuery", () => {
  it("Passes the query data to the decorated component", async () => {
    base.query = jest.fn(() => {
      return { text: "hello" };
    });
    const Component = withQuery(({ data }) => (
      <div>{data.data && <p data-testid="queryData">{data.data.text}</p>}</div>
    ));

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
        options={{}}
      >
        <InteractionProvider>
          {(data) => (
            <Component
              data={data}
              subject="Ellie"
              relationship="Speaks"
              object="French"
            />
          )}
        </InteractionProvider>
      </RainbirdProvider>,
    );

    await waitFor(() => {
      expect(base.query).toHaveBeenCalledTimes(1);
      expect(base.query).toHaveBeenCalledWith(
        "/rainbird",
        "thisIsTheSessionID",
        "Ellie",
        "Speaks",
        "French",
        {},
      );
      const queryData = screen.getByTestId("queryData");
      expect(queryData.textContent).toEqual("hello");
    });
  });
});
