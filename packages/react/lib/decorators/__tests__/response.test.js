import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider, InteractionProvider } from "../../context";
import { withResponse } from "../response";

jest.mock("@rainbird/sdk");

describe("withResponse", () => {
  it("Passes the response function to the decorated component", async () => {
    const Component = withResponse(({ sendResponse }) => (
      <button type="button" data-testid="response" onClick={sendResponse}>
        response
      </button>
    ));

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
      >
        <InteractionProvider>{() => <Component />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("response");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.response).toHaveBeenCalledTimes(1);
    });
  });
});
