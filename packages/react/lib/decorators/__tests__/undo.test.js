import React from "react";
import { render, waitFor, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider, InteractionProvider } from "../../context";
import { withUndo } from "../undo";

jest.mock("@rainbird/sdk");

describe("withUndo", () => {
  it("Passes the undo function to the decorated component", async () => {
    const Component = withUndo(({ undo }) => (
      <button type="button" data-testid="undo" onClick={undo}>
        undo
      </button>
    ));

    render(
      <RainbirdProvider
        kmID="1234"
        apiKey="myApiKey"
        baseURL="/rainbird"
        sessionID="thisIsTheSessionID"
      >
        <InteractionProvider>{() => <Component />}</InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("undo");
    fireEvent.click(button);
    await waitFor(() => {
      expect(base.undo).toHaveBeenCalledTimes(1);
    });
  });
});
