import React from "react";
import { render, waitFor, screen } from "@testing-library/react";

import { InteractionProvider } from "../../context";
import { withInteraction } from "../interaction";

describe("withInteraction", () => {
  it("Passes the interaction data to the decorated component", async () => {
    const Component = withInteraction(({ interaction }) => (
      <div>{interaction.loading && <p data-testid="loading">loading</p>}</div>
    ));

    render(
      <InteractionProvider>
        {() => (
          <Component subject="Ellie" relationship="Speaks" object="French" />
        )}
      </InteractionProvider>,
    );

    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });
});
