import React from "react";

import { useStart } from "../hooks";

export const withStart = (WrappedComponent) => (props) => {
  const start = useStart();
  return <WrappedComponent {...props} start={start} />;
};
