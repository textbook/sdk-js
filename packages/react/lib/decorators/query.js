import React from "react";

import { useQuery } from "../hooks";

export const withQuery = (WrappedComponent) => ({
  subject,
  object,
  relationship,
  ...props
}) => {
  const query = useQuery(subject, relationship, object);
  return <WrappedComponent {...props} query={query} />;
};
