import { useInteraction } from "../hooks";

export const Interaction = ({ children }) => {
  const data = useInteraction();

  return children ? children({ ...data }) : null;
};
