import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider, InteractionProvider } from "../../context";
import { Response } from "../Response";

jest.mock("@rainbird/sdk");

describe("Response", () => {
  afterEach(() => jest.clearAllMocks());

  it("Does not crash if there are no children", () => {
    render(
      <RainbirdProvider>
        <InteractionProvider>{() => <Response />}</InteractionProvider>
      </RainbirdProvider>,
    );
  });

  it("Passes children the response function", () => {
    render(
      <RainbirdProvider>
        <InteractionProvider>
          {() => (
            <Response>
              {(response) => (
                <button
                  type="button"
                  data-testid="response"
                  onClick={response.request}
                >
                  response
                </button>
              )}
            </Response>
          )}
        </InteractionProvider>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("response");
    fireEvent.click(button);
    expect(base.response).toHaveBeenCalled();
  });
});
