import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { Undo } from "..";

jest.mock("@rainbird/sdk");

describe("Undo", () => {
  afterEach(() => jest.clearAllMocks());

  it("Does not crash if there are no children", () => {
    render(
      <RainbirdProvider>
        <Undo />
      </RainbirdProvider>,
    );
  });

  it("Passes children the undo function", () => {
    render(
      <RainbirdProvider>
        <Undo>
          {(undo) => (
            <button type="button" data-testid="undo" onClick={undo.request}>
              undo
            </button>
          )}
        </Undo>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("undo");
    fireEvent.click(button);
    expect(base.undo).toHaveBeenCalled();
  });
});
