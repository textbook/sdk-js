import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { RainbirdProvider } from "../../context";
import { Inject } from "../Inject";

jest.mock("@rainbird/sdk");

describe("Inject", () => {
  afterEach(() => jest.clearAllMocks());

  it("Does not crash if there are no children", () => {
    render(
      <RainbirdProvider>
        <Inject />
      </RainbirdProvider>,
    );
  });

  it("Passes children the Inject function", () => {
    render(
      <RainbirdProvider>
        <Inject>
          {(inject) => (
            <button type="button" data-testid="inject" onClick={inject.request}>
              inject
            </button>
          )}
        </Inject>
      </RainbirdProvider>,
    );

    const button = screen.getByTestId("inject");
    fireEvent.click(button);
    expect(base.inject).toHaveBeenCalled();
  });
});
