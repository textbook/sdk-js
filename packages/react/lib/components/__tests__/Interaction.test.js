import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import { InteractionProvider } from "../../context";
import { Interaction } from "../Interaction";

describe("Interaction", () => {
  it("Does not crash if there are no children", () => {
    render(<InteractionProvider>{() => <Interaction />}</InteractionProvider>);
  });

  it("Provides the default context object to children", async () => {
    let context;
    render(
      <InteractionProvider>
        {() => (
          <Interaction>
            {(interaction) => (
              <div>
                <button
                  type="button"
                  onClick={() => {
                    context = interaction;
                  }}
                  data-testid="setContext"
                >
                  Context
                </button>
              </div>
            )}
          </Interaction>
        )}
      </InteractionProvider>,
    );
    const button = screen.getByTestId("setContext");
    fireEvent.click(button);
    expect(context).toEqual({
      data: null,
      loading: true,
      error: false,
      setInteraction: expect.any(Function),
    });
  });
});
