import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import * as base from "@rainbird/sdk";

import { InteractionProvider } from "../../context";
import { Query } from "..";

jest.mock("@rainbird/sdk");

describe("Query", () => {
  afterEach(() => jest.clearAllMocks());

  it("Does not error if no children are provided", () => {
    render(<InteractionProvider>{() => <Query />}</InteractionProvider>);
  });

  it("Renders loading state when loading", async () => {
    base.query = jest.fn(async () => {});

    render(
      <InteractionProvider>
        {(query) => (
          <Query>
            {() => (
              <>
                {query.loading ? (
                  <p data-testid="loading">loading</p>
                ) : (
                  <p>query</p>
                )}
              </>
            )}
          </Query>
        )}
      </InteractionProvider>,
    );

    await waitFor(() => {
      const loading = screen.getByTestId("loading");
      expect(loading.textContent).toEqual("loading");
    });
  });

  it("Renders the onError prop with the error message when an error is thrown", async () => {
    base.query = jest.fn(async () => {
      throw new Error("error encountered");
    });

    render(
      <InteractionProvider>
        {(query) => (
          <Query>
            {() => (
              <>
                {query.error ? <p data-testid="error">error</p> : <p>query</p>}
              </>
            )}
          </Query>
        )}
      </InteractionProvider>,
    );

    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error");
    });
  });

  it("Renders data if response comes back with data", async () => {
    base.query = jest.fn(async () => {
      return { text: "hello" };
    });

    render(
      <InteractionProvider>
        {(query) => (
          <Query>
            {() => (
              <div>
                {query.data ? (
                  <p data-testid="success">{query.data.text}</p>
                ) : (
                  <p>query</p>
                )}
              </div>
            )}
          </Query>
        )}
      </InteractionProvider>,
    );

    await waitFor(() => {
      const success = screen.getByTestId("success");
      expect(success.textContent).toEqual("hello");
    });
  });
});
