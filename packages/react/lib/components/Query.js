import { useQuery } from "../hooks";

export const Query = ({ subject, relationship, object, children }) => {
  const data = useQuery(subject, relationship, object);

  return children ? children(data) : null;
};
