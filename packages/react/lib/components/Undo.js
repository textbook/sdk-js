import { useUndo } from "../hooks";

export const Undo = ({ children }) => {
  const request = useUndo();

  return children ? children({ request }) : null;
};
