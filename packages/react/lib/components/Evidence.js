import { useEvidence } from "../hooks";

export const Evidence = ({ children }) => {
  const [data, request] = useEvidence();

  return children ? children({ ...data, request }) : null;
};
