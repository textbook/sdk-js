import React from "react";
import { Query } from "./Query";
import { Start } from "./Start";
import { InteractionProvider, RainbirdProvider } from "../context";

export const Rainbird = ({
  onError,
  onLoad,
  baseURL,
  kmID,
  apiKey,
  options,
  kmVersionID,
  sessionID,
  subject,
  relationship,
  object,
  children,
}) => (
  <RainbirdProvider
    baseURL={baseURL}
    kmID={kmID}
    apiKey={apiKey}
    options={options}
    kmVersionID={kmVersionID}
    sessionID={sessionID}
  >
    <Start onLoad={onLoad} onError={onError}>
      {(start) => (
        <>
          {start.loading && <>{onLoad}</>}
          {start.error && <>{onError(start.error)}</>}
          {start.data && (
            <InteractionProvider>
              {({ data, loading, error }) => (
                <Query
                  subject={subject}
                  relationship={relationship}
                  object={object}
                  onLoad={onLoad}
                  onError={onError}
                >
                  {() => (
                    <>
                      {loading && <>{onLoad}</>}
                      {error && <>{onError(error)}</>}
                      {data && <>{children ? children(data) : null}</>}
                    </>
                  )}
                </Query>
              )}
            </InteractionProvider>
          )}
        </>
      )}
    </Start>
  </RainbirdProvider>
);
