import React, { createContext, useState, useEffect } from "react";

// Testing context creation is hard and this function is simple
/* istanbul ignore next */
export const RainbirdContext = createContext({
  apiKey: "",
  baseURL: "",
  kmID: "",
  sessionID: "",
});

export const RainbirdProvider = ({
  apiKey,
  baseURL,
  children,
  kmID,
  kmVersionID,
  options,
  sessionID,
}) => {
  const [rbApiKey, setRbApiKey] = useState(apiKey);
  const [rbBaseURL, setRbBaseURL] = useState(baseURL);
  const [rbKmID, setRbKmID] = useState(kmID);
  const [kmVersion, setKmVersion] = useState(kmVersionID);
  const [internalSessionID, setSessionID] = useState(sessionID);

  useEffect(() => {
    setRbApiKey(apiKey);
    setRbBaseURL(baseURL);
    setRbKmID(kmID);
    setKmVersion(kmVersionID);
    setSessionID(sessionID);
  }, [apiKey, baseURL, kmID, kmVersionID, sessionID]);

  return (
    <RainbirdContext.Provider
      value={{
        apiKey: rbApiKey,
        baseURL: rbBaseURL,
        kmID: rbKmID,
        kmVersion,
        options,
        sessionID: internalSessionID,
        setKmVersion,
        setSessionID,
      }}
    >
      {children}
    </RainbirdContext.Provider>
  );
};
