import React, { createContext, useReducer } from "react";

// Testing context creation is hard and this function is simple
/* istanbul ignore next */
export const InteractionContext = createContext({
  data: null,
  loading: true,
  error: false,
  setInteraction: () => {},
});

const reducer = (_, action) => {
  if (action.loading) return { loading: true, error: false, data: null };
  if (action.error) return { loading: false, error: action.error, data: null };
  return { loading: false, error: false, data: action.payload };
};

export const InteractionProvider = ({ children }) => {
  const [interaction, setInteraction] = useReducer(reducer, {
    loading: true,
    error: false,
    data: null,
  });

  return (
    <InteractionContext.Provider value={{ ...interaction, setInteraction }}>
      {children ? children(interaction) : null}
    </InteractionContext.Provider>
  );
};
