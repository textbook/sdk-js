import React from "react";
import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
} from "@material-ui/core";

export const SingleTruth = ({
  value,
  onChange,
  formContolProps = {},
  radioControlProps = {},
  formControlLabelProps = {},
  radioProps = {},
  ...props
}) => (
  <FormControl component="fieldset" {...formContolProps} {...props}>
    <RadioGroup
      title="Truth select"
      aria-label="Truth select"
      name="Truth select"
      value={value}
      onChange={onChange}
      {...radioControlProps}
    >
      <FormControlLabel
        value="true"
        control={<Radio {...radioProps} />}
        label="True"
        {...formControlLabelProps}
      />
      <FormControlLabel
        value="false"
        control={<Radio {...radioProps} />}
        label="False"
        {...formControlLabelProps}
      />
    </RadioGroup>
  </FormControl>
);
