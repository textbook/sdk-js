import React from "react";
import { Autocomplete } from "@material-ui/lab";
import { InputBase } from "./InputBase";

export const SingleString = ({ options = [], value, onChange, ...props }) => (
  <Autocomplete
    title="Single string select"
    aria-label="Single string select"
    value={value}
    options={options}
    getOptionLabel={(option) => option.name}
    onChange={onChange}
    renderOption={(option) => option.name}
    renderInput={(params) => <InputBase {...params} />}
    {...props}
  />
);
