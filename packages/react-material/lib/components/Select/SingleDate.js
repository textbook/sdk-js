import React from "react";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DayJsUtils from "@date-io/dayjs";

export const SingleDate = ({
  onChange,
  value,
  title = "Date input",
  id = "dateInput",
  ariaLabel = "Date input",
  ...props
}) => (
  // label is associated with control but linting falls over
  // eslint-disable-next-line jsx-a11y/label-has-associated-control
  <label htmlFor={id} aria-label={ariaLabel}>
    <MuiPickersUtilsProvider utils={DayJsUtils}>
      <KeyboardDatePicker
        title={title}
        id={id}
        aria-label={ariaLabel}
        value={value}
        fullWidth
        onChange={onChange}
        {...props}
      />
    </MuiPickersUtilsProvider>
  </label>
);
