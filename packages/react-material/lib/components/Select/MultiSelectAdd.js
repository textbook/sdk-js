import React from "react";
import { TextField, Chip } from "@material-ui/core";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";
import { maybeToString } from "../../utils/utils";

const filter = createFilterOptions();

const MultiSelectAdd = ({
  title,
  onChange,
  question,
  response,
  target,
  ...props
}) => {
  // ToDo refactor this component and MultiSelect to share more code.

  const knownAnswers =
    question.knownAnswers && question.knownAnswers[0]
      ? question.knownAnswers.map((a) => maybeToString(a[target]))
      : [];
  const opts =
    question.concepts && question.concepts.length > 0
      ? question.concepts.map((c) => ({
          name: maybeToString(c.value),
          value: maybeToString(c.value),
        }))
      : [];
  const value =
    response[0] && response[0][target]
      ? response.map((r) => ({
          name: maybeToString(r[target]),
          value: maybeToString(r[target]),
        }))
      : [];

  const handleChange = (e, v) => {
    onChange(
      e,
      v.filter((o) => typeof o !== "string" && !knownAnswers.includes(o.value)),
    );
  };

  const renderInput = (params) => (
    <TextField
      {...params}
      label="Answer"
      variant="outlined"
      placeholder="Answer"
    />
  );

  const getTags = (tagValue, getTagProps) =>
    tagValue.map((option, index) => (
      <Chip
        label={option.name}
        {...getTagProps({ index })}
        disabled={knownAnswers.includes(option.name)}
      />
    ));

  const filterOptions = (options, params) => {
    const filtered = filter(options, params);

    if (params.inputValue !== "") {
      filtered.push({
        known: false,
        overrideTitle: `Add "${params.inputValue}"`,
        value: params.inputValue,
      });
    }

    return filtered;
  };

  return (
    <Autocomplete
      title={title}
      aria-label={title}
      multiple
      value={[...opts.filter((o) => knownAnswers.includes(o.value)), ...value]}
      options={opts}
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === "string") {
          return option;
        }

        // // Add "xxx" option created dynamically
        if (option.overrideTitle) {
          return option.overrideTitle;
        }
        // Regular option
        return option.name;
      }}
      getOptionSelected={(opt, v) => opt.name === v.name}
      disableCloseOnSelect
      onChange={handleChange}
      filterOptions={filterOptions}
      renderTags={getTags}
      renderInput={renderInput}
      {...props}
    />
  );
};

export const MultiStringAdd = ({
  onChange,
  question,
  response,
  target,
  ...props
}) =>
  MultiSelectAdd({
    title: "Multi string select",
    onChange,
    question,
    response,
    target,
    ...props,
  });

export const MultiNumberAdd = ({
  onChange,
  question,
  response,
  target,
  ...props
}) =>
  MultiSelectAdd({
    title: "Multi number select",
    onChange,
    question,
    response,
    target,
    ...props,
  });
