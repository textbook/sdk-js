import React from "react";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";

import { InputBase } from "./InputBase";

const filter = createFilterOptions();

export const SingleStringAdd = ({ options, value, onChange, ...props }) => (
  <Autocomplete
    title="Single string select"
    aria-label="Single string select"
    value={value}
    options={options || []}
    getOptionLabel={(option) => {
      // Value selected with enter, right from the input
      if (typeof option === "string") {
        return option;
      }

      // // Add "xxx" option created dynamically
      if (option.overrideTitle) {
        return option.overrideTitle;
      }
      // Regular option
      return option.name;
    }}
    onChange={onChange}
    filterOptions={(opts, params) => {
      const filtered = filter(opts, params);

      if (params.inputValue !== "") {
        filtered.push({
          value: params.inputValue,
          name: `Add "${params.inputValue}"`,
          type: "string",
          overrideTitle: params.inputValue,
        });
      }

      return filtered;
    }}
    freeSolo
    renderOption={(option) => option.name}
    renderInput={(params) => <InputBase {...params} />}
    {...props}
  />
);
