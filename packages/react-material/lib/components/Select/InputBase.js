import React from "react";
import { TextField } from "@material-ui/core";

export const InputBase = (props) => (
  <TextField
    {...props}
    label="Answer"
    variant="outlined"
    placeholder="Answer"
  />
);
