import React from "react";
import { TextField, Chip } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { maybeToString } from "../../utils/utils";

const MultiSelect = ({
  title,
  onChange,
  question,
  response,
  target,
  ...props
}) => {
  const knownAnswers =
    question.knownAnswers && question.knownAnswers[0]
      ? question.knownAnswers.map((a) => maybeToString(a[target]))
      : [];
  const opts =
    question.concepts && question.concepts.length > 0
      ? question.concepts.map((c) => ({
          name: maybeToString(c.value),
          value: maybeToString(c.value),
        }))
      : [];
  const value =
    response[0] && response[0][target]
      ? response.map((r) => ({
          name: maybeToString(r[target]),
          value: maybeToString(r[target]),
        }))
      : [];

  const renderInput = (params) => (
    <TextField
      {...params}
      label="Answer"
      variant="outlined"
      placeholder="Answer"
    />
  );

  const renderTags = (tagValue, getTagProps) =>
    tagValue.map((option, index) => (
      <Chip
        label={option.name}
        {...getTagProps({ index })}
        disabled={knownAnswers.includes(option.name)}
      />
    ));

  const handleChange = (e, v) =>
    onChange(
      e,
      v.filter((o) => typeof o !== "string" && !knownAnswers.includes(o.value)),
    );

  const label = title.toLowerCase();

  return (
    <Autocomplete
      {...props}
      aria-label={label}
      getOptionLabel={(option) => option.name}
      getOptionSelected={(opt, v) => opt.name === v.name}
      disableCloseOnSelect
      id={label.split(" ").join("-")}
      multiple
      onChange={handleChange}
      options={opts}
      renderInput={renderInput}
      renderTags={renderTags}
      title={title}
      value={[...opts.filter((o) => knownAnswers.includes(o.value)), ...value]}
    />
  );
};

export const MultiString = ({
  onChange,
  question,
  response,
  target,
  ...props
}) =>
  MultiSelect({
    title: "Multi string select",
    onChange,
    question,
    response,
    target,
    ...props,
  });

export const MultiNumber = ({
  onChange,
  question,
  response,
  target,
  ...props
}) =>
  MultiSelect({
    title: "Multi number select",
    onChange,
    question,
    response,
    target,
    ...props,
  });
