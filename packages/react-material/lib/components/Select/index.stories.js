import React from "react";

import dayjs from "dayjs";
import dayjsPluginUTC from "dayjs-plugin-utc";
import {
  Certainty,
  MultiString,
  MultiStringAdd,
  SingleDate,
  MultiDate,
  SingleNumber,
  MultiNumber,
  MultiNumberAdd,
  SingleString,
  SingleStringAdd,
  SingleTruth,
} from ".";

dayjs.extend(dayjsPluginUTC);

// eslint-disable-next-line import/no-default-export
export default {
  title: "Select",
};

const paddingTop = "50px";

export const Default = () => (
  <div style={{ fontFamily: "Arial" }}>
    <section style={{ paddingTop }}>
      <p>Certainty:</p>
      <Certainty onChange={() => {}} value={50} />
    </section>
    <section style={{ paddingTop }}>
      <p>MultiString:</p>
      <MultiString
        onChange={() => {}}
        question={{
          knownAnswers: [{ object: "Bob" }],
          concepts: [{ value: "Bob" }, { value: "Lucy" }, { value: "Ramesh" }],
        }}
        response={[
          {
            subject: "Someone",
            relationship: "Likes",
            object: "",
          },
        ]}
        target="object"
      />
    </section>
    <section style={{ paddingTop }}>
      <p>MultiStringAdd:</p>
      <MultiStringAdd
        onChange={() => {}}
        question={{
          knownAnswers: [{ object: "Bob" }],
          concepts: [{ value: "Bob" }, { value: "Lucy" }, { value: "Ramesh" }],
        }}
        response={[{ subject: "Someone", relationship: "Likes", object: "" }]}
        target="object"
      />
    </section>
    <section style={{ paddingTop }}>
      <p>MultiNumberAdd:</p>
      <MultiNumberAdd
        onChange={() => {}}
        question={{
          knownAnswers: [{ object: 20 }],
          concepts: [{ value: 10 }, { value: 15 }, { value: 20 }],
        }}
        response={[{ subject: "Someone", relationship: "Age", object: "" }]}
        target="object"
      />
    </section>
    <section style={{ paddingTop }}>
      <p>MultiNumber:</p>
      <MultiNumber
        onChange={() => {}}
        question={{
          knownAnswers: [{ object: 10 }],
          concepts: [{ value: 10 }, { value: 15 }, { value: 20 }],
        }}
        response={[{ subject: "Someone", relationship: "Age", object: "" }]}
        target="object"
      />
    </section>
    <section style={{ paddingTop }}>
      <p>SingleDate:</p>
      <SingleDate onChange={() => {}} />
    </section>
    <section style={{ paddingTop }}>
      <p>MultiDate:</p>
      <MultiDate
        onChange={() => {}}
        question={{
          knownAnswers: [{ object: dayjs(new Date()).valueOf() }],
          concepts: [],
        }}
        response={[{ subject: "Someone", relationship: "Age", object: "" }]}
        target="object"
      />
    </section>
    <section style={{ paddingTop }}>
      <p>SingleNumber:</p>
      <SingleNumber onChange={() => {}} />
    </section>
    <section style={{ paddingTop }}>
      <p>SingleString:</p>
      <SingleString
        onChange={() => {}}
        options={[{ name: "Bob" }, { name: "Lucy" }, { name: "Ramesh" }]}
      />
    </section>
    <section style={{ paddingTop }}>
      <p>SingleStringAdd:</p>
      <SingleStringAdd
        onChange={() => {}}
        options={[{ name: "Bob" }, { name: "Lucy" }, { name: "Ramesh" }]}
      />
    </section>
    <section style={{ paddingTop }}>
      <p>SingleTruth:</p>
      <SingleTruth onChange={() => {}} />
    </section>
  </div>
);
