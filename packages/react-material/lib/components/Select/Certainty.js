import React from "react";
import { Slider } from "@material-ui/core";

export const Certainty = ({ onChange, value, ...props }) => (
  <Slider
    aria-label="Certainty slider"
    title="Certainty slider"
    value={value}
    onChange={onChange}
    valueLabelDisplay="auto"
    {...props}
  />
);
