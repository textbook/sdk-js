import React from "react";
import { TextField } from "@material-ui/core";

export const SingleNumber = ({ onChange, value, ...props }) => (
  // label is associated with control but linting falls over
  // eslint-disable-next-line jsx-a11y/label-has-associated-control
  <label aria-label="number input" htmlFor="numberInput">
    <TextField
      id="numberInput"
      aria-label="Number input"
      title="Number input"
      placeholder="Answer"
      type="number"
      value={value}
      fullWidth
      onChange={onChange}
      {...props}
    />
  </label>
);
