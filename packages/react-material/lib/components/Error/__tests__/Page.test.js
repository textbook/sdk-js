import React from "react";
import { screen, render } from "@testing-library/react";
import { ErrorPage } from "..";

describe("Error Page", () => {
  it("The refresh and report buttons call the correct props", () => {
    const onRefresh = jest.fn();
    const onReport = jest.fn();
    render(
      <ErrorPage
        onRefresh={onRefresh}
        onReport={onReport}
        error={{ message: "An error occurred" }}
      />,
    );
    screen.getByTestId("error-refresh").click();
    screen.getByTestId("error-report").click();

    expect(onRefresh).toHaveBeenCalled();
    expect(onReport).toHaveBeenCalled();
  });
});
