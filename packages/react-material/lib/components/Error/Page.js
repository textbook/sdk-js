import React from "react";
import { Typography, Box, Button } from "@material-ui/core";

export const ErrorPage = ({ onRefresh, onReport, error }) => (
  <Box
    padding={3}
    display="flex"
    justifyContent="center"
    alignItems="center"
    flexDirection="column"
  >
    <Typography data-testid="error-title" variant="h5">
      An error occurred!
    </Typography>
    <Typography data-testid="error-message" variant="caption">
      {error.message}
    </Typography>

    <Box padding={3}>
      <Button
        aria-label="Refresh page"
        color="primary"
        data-testid="error-refresh"
        onClick={onRefresh}
        title="Refresh"
        type="button"
        style={{ marginRight: "10px" }}
        variant="outlined"
      >
        Refresh
      </Button>
      <Button
        aria-label="Refresh page"
        color="primary"
        data-testid="error-report"
        onClick={onReport}
        title="Refresh"
        type="button"
        style={{ marginLeft: "10px" }}
        variant="contained"
      >
        Report
      </Button>
    </Box>
  </Box>
);
