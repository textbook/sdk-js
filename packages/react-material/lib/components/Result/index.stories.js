import React from "react";
import { Result } from ".";

// eslint-disable-next-line import/no-default-export
export default {
  title: "Result",
  component: Result,
  argTypes: {
    data: { type: "array" },
  },
};

const Template = (args) => <Result {...args} />;

export const Default = Template.bind({});
Default.args = {
  data: [
    {
      subject: "Bob",
      relationship: "Speaks",
      object: "French",
      certainty: 100,
    },
    {
      subject: "Bob",
      relationship: "Speaks",
      object: "English",
      certainty: 60,
    },
    {
      subject: "Bob",
      relationship: "Speaks",
      object: "Spanish",
      certainty: 80,
    },
  ],
};
