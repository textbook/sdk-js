import React from "react";
import { Typography, Button, Box, Grid } from "@material-ui/core";

export const Result = ({ data, reset }) => (
  <Grid
    container
    direction="column"
    alignItems="center"
    style={{ position: "relative", height: "100%" }}
  >
    <Grid role="main" container direction="column" justify="flex-start">
      {data.length ? (
        <Typography aria-level="2" role="heading" variant="h5">
          Your results:
        </Typography>
      ) : (
        <Typography data-testid="error">
          Sorry I&apos;ve been unable to find an answer to your question!
        </Typography>
      )}
      <ul>
        {data.map((d) => {
          const result = `${d.subject} ${d.relationship} ${d.object} - ${d.certainty}%`;
          return (
            <li key={result}>
              <Typography aria-label="Results" data-testid="result">
                {result}
              </Typography>
            </li>
          );
        })}
      </ul>
    </Grid>
    <Grid
      container
      direction="row"
      justify="center"
      style={{ position: "absolute", bottom: "-1.6rem" }}
    >
      <Box p={3}>
        <Button
          aria-label="Reset back to configuration"
          color="primary"
          data-testid="reset-response"
          onClick={reset}
          title="Reset back to configuration"
          type="button"
          variant="outlined"
        >
          Reset
          <span style={{ display: "none" }}>back to configuration</span>
        </Button>
      </Box>
    </Grid>
  </Grid>
);
