import React, { useState } from "react";
import {
  Paper,
  AppBar,
  ThemeProvider,
  Grid,
  Box,
  Typography,
} from "@material-ui/core";
import { RainbirdLogo } from "./RainbirdLogo";
import { RBTheme } from "../../styles";
import { Rainbird } from "../Rainbird";
import { Configuration } from "../Configuration";
import { ErrorPage } from "../Error";

export const KitchenSink = ({
  apiKey,
  baseURL,
  kmID,
  subject: origSubject,
  relationship,
  object: origObject,
  theme = RBTheme,
  options,
}) => {
  const [subject, setSubject] = useState(origSubject);
  const [object, setObject] = useState(origObject);
  const [resetKey, setResetKey] = useState();
  // If subject or object is the same with original state
  // we need to have a random key for a reset
  const reset = () => {
    setResetKey(Date.now());
    setSubject(origSubject);
    setObject(origObject);
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container direction="column" alignItems="center">
        <Grid
          aria-label="Banner"
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
        >
          <RainbirdLogo />
        </Grid>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          style={{ height: "80vh", minHeight: "500px" }}
        >
          <Box p={3}>
            <Paper>
              <Box width="80vw">
                <AppBar position="relative" color="primary">
                  <Box p={1.5}>
                    <Typography
                      aria-level="1"
                      role="heading"
                      data-testid="query-name"
                      component="h1"
                      variant="body1"
                    >
                      Query:
                      {` ${subject || "?"} ${relationship} ${object || "?"}`}
                    </Typography>
                  </Box>
                </AppBar>
                <Box
                  role="main"
                  p={4}
                  style={{
                    overflow: "auto",
                    height: "55vh",
                    minHeight: "400px",
                    maxHeight: "800px",
                  }}
                >
                  {!subject && !object && (
                    <Configuration subject={setSubject} object={setObject} />
                  )}
                  {(subject || object) && (
                    <Rainbird
                      apiKey={apiKey}
                      baseURL={baseURL}
                      kmID={kmID}
                      key={resetKey}
                      subject={subject}
                      relationship={relationship}
                      object={object}
                      theme={theme}
                      options={options}
                      reset={reset}
                      onError={
                        /* istanbul ignore next: impossible to test without adding extra props to force throw errors */ (
                          err,
                          errInfo,
                        ) => (
                          <ErrorPage
                            error={err}
                            errorInfo={errInfo}
                            onRefresh={() => window.location.reload()}
                            onReport={() => {
                              window.open(
                                `mailto:support@rainbird-example.com?subject=${"Rainbird Kitchen Sink Error"}&body=${
                                  err.message
                                }\n\n${errInfo.componentStack}`,
                              );
                            }}
                          />
                        )
                      }
                    />
                  )}
                </Box>
              </Box>
            </Paper>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
};
