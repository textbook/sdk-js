import React from "react";
import { KitchenSink } from ".";

// eslint-disable-next-line import/no-default-export
export default {
  title: "Kitchen Sink",
  component: KitchenSink,
  argTypes: {
    apiKey: { type: "text" },
    baseURL: { type: "text" },
    kmID: { type: "text" },
    subject: { type: "text" },
    relationship: { type: "text" },
    object: { type: "text" },
  },
};

const Template = (args) => <KitchenSink {...args} />;

export const Default = Template.bind({});

Default.args = {
  apiKey: "",
  baseURL: "",
  kmID: "",
  subject: "",
  relationship: "",
  object: "",
};
