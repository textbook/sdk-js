import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
// eslint-disable-next-line import/no-extraneous-dependencies
import * as base from "@rainbird/sdk";

import { KitchenSink } from "..";

jest.mock("@rainbird/sdk");

describe("Kitchen Sink", () => {
  afterEach(() => jest.clearAllMocks());

  it("Displays the heading for the query", () => {
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: Ellie speaks French");
  });

  it("Displays config to fill in subject and/or object proceeding to KitchenSink", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: { prompt: "What country does Christiaan live in?" },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject=""
        relationship="speaks"
        object=""
      />,
    );
    let submit;
    let subjectInput;
    await waitFor(() => {
      subjectInput = screen
        .getByTestId("subject-pre-query")
        .querySelector("input");
      submit = screen.getByTestId("submit-config");
    });
    fireEvent.change(subjectInput, { target: { value: "Christiaan" } });
    fireEvent.click(submit);
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: Christiaan speaks ?");
  });

  it("Displays question marks in the query heading when subject or object is absent", () => {
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject=""
        relationship="speaks"
        object=""
      />,
    );
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: ? speaks ?");
  });

  it("Displays the loading spinner when waiting for a response from start", () => {
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    screen.getByTestId("loading");
  });

  it("Displays the loading spinner when waiting for a response from query", () => {
    base.start = jest.fn(() => {
      return {};
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    screen.getByTestId("loading");
  });

  it("Displays error when an error is thrown", () => {
    base.start = jest.fn(() => {
      throw new Error("Error starting session");
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    const error = screen.getByTestId("error");
    expect(error.textContent).toEqual("Error starting session");
  });

  it("Displays the question name when returned from query", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: { prompt: "What country does Ellie live in?" },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const prompt = screen.getByTestId("prompt");
      expect(prompt.textContent).toEqual("What country does Ellie live in?");
    });
  });

  it("Displays an error when an error is returned from query", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      throw new Error("error!");
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const error = screen.getByTestId("error");
      expect(error.textContent).toEqual("error!");
    });
  });

  it("Displays the back button and a disabled submit button on mount", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: { prompt: "What country does Ellie live in?" },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const back = screen.getByTestId("undo-response");
      expect(back.textContent).toEqual("Back to previous question");
      const submit = screen.getByTestId("submit-response");
      expect(submit.disabled).toEqual(true);
      expect(submit.textContent).toEqual("Submit response");
    });
  });

  it("Displays the skip button when a question is skippable", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    await waitFor(() => {
      const skip = screen.getByTestId("skip-question");
      expect(skip.textContent).toEqual("Skip");
    });
  });

  it("Clicking undo calls the undo endpoint", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: { prompt: "What country does Ellie live in?" },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.undo = jest.fn();
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let back;
    await waitFor(() => {
      back = screen.getByTestId("undo-response");
    });
    fireEvent.click(back);
    expect(base.undo).toHaveBeenCalled();
  });

  it("Clicking submit calls the response endpoint with the correct data", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            prompt: "What country does Ellie live in?",
            object: "French",
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn();
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let respond;
    await waitFor(() => {
      respond = screen.getByTestId("submit-response");
    });
    fireEvent.submit(respond);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [{ cf: 100, object: "French", relationship: "speaks", subject: "Ellie" }],
      undefined,
    );
  });

  it("Clicking skip calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            object: "French",
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          object: "French",
          relationship: "speaks",
          subject: "Ellie",
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Clicking skip with no subject calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            object: "French",
            relationship: "speaks",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        relationship="speaks"
        object="French"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          object: "French",
          relationship: "speaks",
          subject: "",
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Clicking skip with no object calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        relationship="speaks"
        subject="Ellie"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          subject: "Ellie",
          relationship: "speaks",
          object: "",
          unanswered: true,
        },
      ],
      undefined,
    );
  });

  it("Clicking skip with extra questions calls the response endpoint with unanswered: true", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: {
            prompt: "What country does Ellie live in?",
            allowUnknown: true,
            object: "French",
            relationship: "speaks",
            subject: "Ellie",
          },
          extraQuestions: [
            {
              prompt: "What country does Ellie live in?",
              allowUnknown: true,
              object: "English",
              relationship: "speaks",
              subject: "Ellie",
            },
          ],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    base.response = jest.fn(() => {});
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject="Ellie"
        relationship="speaks"
        object="French"
      />,
    );
    let skip;
    await waitFor(() => {
      skip = screen.getByTestId("skip-question");
    });
    fireEvent.click(skip);
    expect(base.response).toHaveBeenCalledWith(
      "https://api.rainbird.ai",
      undefined,
      [
        {
          cf: 100,
          object: "French",
          relationship: "speaks",
          subject: "Ellie",
          unanswered: true,
        },
        [
          {
            cf: 100,
            object: "English",
            relationship: "speaks",
            subject: "Ellie",
            unanswered: true,
          },
        ],
      ],
      undefined,
    );
  });
  it("Should not have subject or object, proceed with query and reset approriately", async () => {
    base.start = jest.fn(() => ({ data: {} }));
    base.query = jest.fn(() => {
      return {
        data: {
          question: { prompt: "What country does Christiaan live in?" },
          extraQuestions: [],
        },
        type: base.RESPONSE_TYPE_QUESTION,
      };
    });
    render(
      <KitchenSink
        baseURL="https://api.rainbird.ai"
        apiKey="apiKey"
        kmID="12345"
        subject=""
        relationship="speaks"
        object=""
      />,
    );
    let submit;
    let subjectInput;
    let resetButton;
    await waitFor(() => {
      subjectInput = screen
        .getByTestId("subject-pre-query")
        .querySelector("input");
      submit = screen.getByTestId("submit-config");
    });
    fireEvent.change(subjectInput, { target: { value: "Christiaan" } });
    fireEvent.click(submit);
    const query = screen.getByTestId("query-name");
    expect(query.textContent).toEqual("Query: Christiaan speaks ?");
    await waitFor(() => {
      resetButton = screen.getByTestId("reset-response");
    });
    fireEvent.click(resetButton);
    expect(query.textContent).toEqual("Query: ? speaks ?");
  });
});
