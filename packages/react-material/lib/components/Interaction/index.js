import React, { useState } from "react";
import ReactMarkdown from "react-markdown";
import { Snackbar, Typography, Box, Grid, Button } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { FormControl, useResponse, useUndo } from "@rainbird/sdk-react";
import dayjs from "dayjs";
import dayjsPluginUTC from "dayjs-plugin-utc";

import {
  SingleNumber,
  SingleStringAdd,
  SingleString,
  Certainty,
  MultiStringAdd,
  MultiString,
  SingleDate,
  MultiDate,
  SingleTruth,
  InputBase,
  MultiNumber,
  MultiNumberAdd,
} from "../Select";
import { toTimestampAtStartOfDay } from "../../utils/utils";

dayjs.extend(dayjsPluginUTC);

export const Interaction = ({ data, error, reset}) => {
  const { question, extraQuestions } = data;
  const respond = useResponse();
  const undo = useUndo();

  const initialState = data.question
    ? [
        [
          {
            cf: 100,
            object: question.object,
            relationship: question.relationship,
            subject: question.subject,
          },
        ],
        ...extraQuestions.map(({ subject, relationship, object }) => [
          { subject, relationship, object, cf: 100 },
        ]),
      ]
    : undefined;
  const [response, setResponse] = useState(initialState);

  const getQuestionTarget = (q) => {
    const qType = q.type && q.type.toLowerCase();
    if (qType === "first form") return "answer";
    return qType === "second form object" ? "object" : "subject";
  };

  const submitDisabled =
    response &&
    response
      .map((group, i) =>
        group
          .map((r) => {
            const q = i > 0 ? extraQuestions[i - 1] : question;
            const target = r[getQuestionTarget(q)];
            const knownAnswers =
              Array.isArray(q.knownAnswers) && q.knownAnswers.length;
            if ((target === undefined && q.allowUnknown) || knownAnswers)
              return false;
            if (target === undefined) return true;
            return false;
          })
          .includes(true),
      )
      .includes(true);

  const submit = (e) => {
    e.preventDefault();
    if (submitDisabled) return;
    const res = flattenResponse().map((r) => {
      let item = r;
      const unanswered =
        (!r.subject && typeof r.subject !== "boolean") ||
        (!r.object && typeof r.object !== "boolean");
      if (unanswered) item.unanswered = true;
      return item;
    });
    respond(res);
  };

  const handleSkip = () =>
    respond([
      {
        cf: 100,
        object: question.object || "",
        relationship: question.relationship,
        subject: question.subject || "",
        unanswered: true,
      },
      ...extraQuestions.map(({ subject, relationship, object }) => [
        {
          cf: 100,
          object,
          relationship,
          subject,
          unanswered: true,
        },
      ]),
    ]);

  const flattenResponse = () =>
    response.reduce((result, g) => result.concat(g), []);

  // Rebuild res and update the target question group
  const handleResChange = (group, i) =>
    setResponse((prevRes) =>
      prevRes.map((prevGroup, j) => (i === j ? group : prevGroup)),
    );

  return (
    <form
      style={{
        alignItems: "center",
        display: "flex",
        height: "100%",
        justifyContent: "center",
      }}
      onSubmit={submit}
    >
      {error && (
        <Snackbar open anchorOrigin={{ horizontal: "center", vertical: "top" }}>
          <Alert data-testid="error" severity="error">
            {error.map((e) => (
              <Typography key={e}>{e}</Typography>
            ))}
          </Alert>
        </Snackbar>
      )}
      {data && data.question && (
        <Grid
          container
          direction="column"
          justify="space-between"
          style={{ height: "100%" }}
          wrap="nowrap"
        >
          <BoxContainer
            question={question}
            response={response[0]}
            handleResChange={(g) => handleResChange(g, 0)}
            target={getQuestionTarget(question)}
          />
          {extraQuestions &&
            extraQuestions.map((q, i) => (
              <BoxContainer
                key={`extraQuestionGroup-${i}`}
                question={q}
                response={response[i + 1]}
                handleResChange={(g) => handleResChange(g, i + 1)}
                target={getQuestionTarget(question)}
              />
            ))}
          <Grid container direction="row" justify="space-between">
            <Button
              aria-label="Back to previous question"
              color="primary"
              data-testid="undo-response"
              onClick={undo}
              title="Back to previous question"
              type="button"
              variant="outlined"
            >
              Back <span style={{ display: "none" }}>to previous question</span>
            </Button>
            <Button
              aria-label="Reset back to configuration"
              color="primary"
              data-testid="reset-response"
              onClick={reset}
              title="Reset back to configuration"
              type="button"
              variant="outlined"
            >
              Reset
              <span style={{ display: "none" }}>back to configuration</span>
            </Button>
            <Grid>
              {question.allowUnknown && (
                <Button
                  color="primary"
                  data-testid="skip-question"
                  onClick={handleSkip}
                  type="button"
                  variant="outlined"
                >
                  Skip
                </Button>
              )}
              <Button
                aria-disabled={submitDisabled}
                aria-label="Submit response"
                color="primary"
                data-testid="submit-response"
                disabled={submitDisabled}
                style={{ marginLeft: "10px" }}
                title="Submit response"
                type="submit"
                variant="contained"
              >
                Submit <span style={{ display: "none" }}>response</span>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      )}
    </form>
  );
};

const BoxContainer = ({ handleResChange, question, response, target }) => {
  // Replace item in the response array on change
  const handleSimpleChange = (_, newValue) => {
    let newResponse = response[0];
    newResponse[target] = (newValue && newValue.value) || undefined;
    return handleResChange([newResponse]);
  };

  // Replace multi-item in the response array on change
  const handleMultiChange = (_, v) => {
    if (v.length === 0) {
      let newResponse = response[0];
      newResponse = [
        {
          subject: question.subject,
          relationship: question.relationship,
          object: question.object,
          cf: 100,
        },
      ];
      return handleResChange(newResponse);
    }

    const uniqueItems = Array.from(new Set(v.map((i) => i.value))).map((name) =>
      v.find((i) => i.value === name),
    );
    const newResponse = uniqueItems.map((inst) => ({
      object: question.object || inst.value,
      relationship: question.relationship,
      subject: question.subject || inst.value,
      cf: response[0] && response[0].cf,
    }));
    return handleResChange(newResponse);
  };

  const handleSingleTruthOnChange = (change) => {
    const { value } = change.target;
    const valueIsTrue = value === "true";
    const answer = valueIsTrue ? "yes" : "no";

    // If the answer is first form, we want to reply yes/no to answer
    const isFirstForm =
      question && question.type && question.type.toLowerCase() === "first form";

    let newResponse = response[0];
    newResponse[target] = isFirstForm ? answer : valueIsTrue;
    return handleResChange([newResponse]);
  };

  return (
    <Box>
      <Typography
        component="h2"
        data-testid="prompt"
        id="prompt"
        variant="body1"
      >
        <ReactMarkdown>{question && question.prompt}</ReactMarkdown>
      </Typography>
      <Grid container direction="column" justify="space-between">
        <Box paddingTop={2} paddingBottom={2}>
          <FormControl
            data={question}
            singleStringAdd={
              question.concepts && question.concepts.length > 0 ? (
                <SingleStringAdd
                  data-testid="single-string-add"
                  value={response[0][target]}
                  options={question.concepts}
                  onChange={handleSimpleChange}
                />
              ) : (
                <InputBase
                  fullWidth
                  onChange={(e) => handleSimpleChange("", e.target)}
                  value={response[0][target]}
                  data-testid="single-string-add-noInsts"
                />
              )
            }
            singleString={
              <SingleString
                data-testid="single-string"
                value={response[0][target]}
                options={question.concepts}
                onChange={handleSimpleChange}
              />
            }
            multiStringAdd={
              <MultiStringAdd
                data-testid="multi-string-add"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            multiString={
              <MultiString
                data-testid="multi-string"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            singleDate={
              <SingleDate
                minDate={new Date("1000/01/01")}
                maxDate={new Date("5000/12/31")}
                data-testid="single-date"
                format="DD/MM/YYYY"
                initialFocusedDate={new Date()}
                value={
                  response[0][target] ? new Date(response[0][target]) : null
                }
                placeholder="dd/mm/yyyy"
                onChange={(date) => {
                  if (!date) return;
                  const newResponse = response[0];
                  newResponse[target] = toTimestampAtStartOfDay(date).timestamp;
                  return handleResChange([newResponse]);
                }}
              />
            }
            multiDate={
              <MultiDate
                data-testid="multi-date"
                question={question}
                response={response}
                onChange={handleMultiChange}
                target={target}
              />
            }
            singleNumber={
              <SingleNumber
                data-testid="single-number"
                value={response[0][target]}
                onChange={(e) => {
                  let newResponse = response[0];
                  newResponse[target] = e.target.value;
                  return handleResChange([newResponse]);
                }}
              />
            }
            multiNumber={
              <MultiNumber
                data-testid="multi-number"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            multiNumberAdd={
              <MultiNumberAdd
                data-testid="multi-number-add"
                question={question}
                onChange={handleMultiChange}
                response={response}
                target={target}
              />
            }
            singleTruth={
              <SingleTruth
                data-testid="single-truth"
                onChange={handleSingleTruthOnChange}
                value={response[0][target]}
              />
            }
            certaintySelect={
              <Box paddingTop={3}>
                <Certainty
                  data-testid="certainty"
                  value={response[0].cf}
                  onChange={(_, v) => {
                    let newResponse = response[0];
                    newResponse.cf = v;
                    return handleResChange([newResponse]);
                  }}
                />
              </Box>
            }
            fallback={<p>something went wrong</p>}
          />
        </Box>
      </Grid>
    </Box>
  );
};
