import React from "react";
import { Configuration } from ".";

// eslint-disable-next-line import/no-default-export
export default {
  title: "Configuration",
  component: Configuration,
  argTypes: {
    apiKey: { type: "text" },
    baseURL: { type: "text" },
    kmID: { type: "text" },
    subject: { type: "text" },
    relationship: { type: "text" },
    object: { type: "text" },
    data: { type: "object" },
  },
};

const Template = (args) => <Configuration {...args} />;

export const Default = Template.bind({});
Default.args = {
  apiKey: "",
  baseURL: "",
  kmID: "",
  subject: "",
  relationship: "",
  object: "",
};
