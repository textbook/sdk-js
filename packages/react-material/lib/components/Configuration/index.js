import React, { useState } from "react";
import { Typography, Box, Button, TextField } from "@material-ui/core";

export const Configuration = ({ subject, object }) => {
  const [configSubject, setConfigSubject] = useState();
  const [configObject, setConfigObject] = useState();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (configSubject) subject(configSubject);
    if (configObject) object(configObject);
  };
  return (
    <>
      <Box
        sx={{
          "& .MuiTextField-root": { m: 1, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
        style={{ height: "calc(100% - 2.3rem)" }}
      >
        <Typography
          component="h2"
          data-testid="prompt"
          id="prompt"
          variant="body1"
        >
          Subject and/or Object not found. Please enter either/both.
        </Typography>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "25ch" },
          }}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <TextField
            fullWidth
            margin="normal"
            id="outlined-basic"
            data-testid="subject-pre-query"
            label="Subject"
            variant="outlined"
            name="subject"
            onChange={(e) => setConfigSubject(e.target.value)}
          />
        </Box>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "25ch" },
          }}
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <TextField
            fullWidth
            margin="normal"
            id="outlined-basic"
            data-testid="object-pre-query"
            label="Object"
            variant="outlined"
            name="object"
            onChange={(e) => setConfigObject(e.target.value)}
          />
        </Box>
      </Box>
      <Button
        aria-disabled={!configSubject && !configObject}
        aria-label="Submit"
        color="primary"
        data-testid="submit-config"
        disabled={!configSubject && !configObject}
        onClick={handleSubmit}
        title="Submit config"
        type="button"
        variant="outlined"
        style={{ float: "right" }}
      >
        Submit
        <span style={{ display: "none" }}>config</span>
      </Button>
    </>
  );
};
