import React from "react";
import { Snackbar, CircularProgress } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import { ThemeProvider } from "@material-ui/core/styles";
// eslint-disable-next-line import/no-extraneous-dependencies
import { RESPONSE_TYPE_QUESTION, RESPONSE_TYPE_RESULT } from "@rainbird/sdk";
// eslint-disable-next-line import/no-extraneous-dependencies
import { Rainbird as ReactRainbird } from "@rainbird/sdk-react";
import { RBTheme } from "../../styles";
import { Interaction } from "../Interaction";
import { Result } from "../Result";
import { ErrorBoundary } from "../Error";

export const Rainbird = ({
  kmID,
  baseURL,
  apiKey,
  options,
  theme = RBTheme,
  subject,
  object,
  relationship,
  reset,
  onError,
}) => {
  return (
    <ThemeProvider theme={theme}>
      <ErrorBoundary onError={onError}>
        <ReactRainbird
          onLoad={
            // disabled because otherwise it complains about parentheses
            // eslint-disable-next-line react/jsx-wrap-multilines
            <CircularProgress
              role="status"
              aria-label="Loading indicator"
              data-testid="loading"
            />
          }
          onError={(e) => (
            <Snackbar
              open
              anchorOrigin={{ horizontal: "center", vertical: "top" }}
            >
              <Alert data-testid="error" severity="error">
                <span role="status">{e.message}</span>
              </Alert>
            </Snackbar>
          )}
          kmID={kmID}
          baseURL={baseURL}
          apiKey={apiKey}
          options={options}
          subject={subject}
          object={object}
          relationship={relationship}
        >
          {({ data, type }) => {
            if (type === RESPONSE_TYPE_QUESTION)
              return <Interaction data={data} reset={reset} />;
            if (type === RESPONSE_TYPE_RESULT)
              return <Result data={data} reset={reset} />;
            return <></>;
          }}
        </ReactRainbird>
      </ErrorBoundary>
    </ThemeProvider>
  );
};
