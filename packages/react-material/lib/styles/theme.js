import { createMuiTheme } from "@material-ui/core/styles";

// Testing this would only test 3rd party software
/* istanbul ignore next */
export const RBTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#6200EE",
    },
    secondary: {
      main: "#4fbbc5",
    },
  },
});
