export const maybeToString = (maybeValue) =>
  maybeValue ? maybeValue.toString() : maybeValue;

export const toTimestampAtStartOfDay = (dateString) => {
  const date = new Date(dateString);
  date.setUTCHours(0, 0, 0, 0);
  return {
    locale: date.toLocaleDateString(),
    timestamp: date.valueOf(),
  };
};
