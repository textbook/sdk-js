import { toTimestampAtStartOfDay } from "./utils";

describe("utils", () => {
  it("converts date to timestamp at start of day", () => {
    const date = "2021-12-01";
    const startOfDay = toTimestampAtStartOfDay(date);
    expect(startOfDay.timestamp).toEqual(1638316800000);
  });
});
