> # @rainbird/sdk-react-material
[![pipeline status](https://gitlab.com/rainbird-ai/sdk-js/badges/master/pipeline.svg)](https://gitlab.com/rainbird-ai/sdk-js/badges/master/pipeline.svg)   [![coverage status](https://gitlab.com/rainbird-ai/sdk-js/badges/master/coverage.svg)](https://gitlab.com/rainbird-ai/sdk-js/badges/master/coverage.svg)

A package that builds on [@rainbird/sdk](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/base) and [@rainbird/sdk-react](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/react) and adds some Material-UI. Best suited for those that want to get up and running as quickly as possible.

> ## Installation

`yarn add @rainbird/sdk-react-material`

`npm i @rainbird/sdk-react-material`

> ## Usage

[Components](#components) | [Styles](#styles)

#### Components
[KitchenSink](#kitchensink) | [Rainbird](#rainbird) | [Interaction](#interaction) | [Result](#decorators) | [Select](#select) | [ErrorBoundary](#errorboundary)

##### KitchenSink
KitchenSink is a component that has been designed so that a whole interaction can be run with Rainbird from start to finish. It can also be customised using a Material-UI theme object or you can rely on our [RBTheme](#rbtheme). To see and interact with the component then install this package and run `yarn storybook`.

```jsx
import React from 'react';
import { createMuiTheme } from "@material-ui/core/styles";
import { KitchenSink } from '@rainbird/sdk-react-material';

const theme = createMuiTheme({
    primary: {
        main: '#1c083b'
    },
    secondary: {
        main: '#4fbbc5'
    }
})

export const App = () => (
    <KitchenSink
        apiKey="myApiKey"
        baseURL="https://test.rainbird.ai"
        kmID="myKmID"
        subject="Bob"
        relationship="speaks"
        object=""
        theme={theme} // optional
        options={{}} // optional
    />
)
```

##### Rainbird
Rainbird is all of KitchenSink but without the outer container styling. It takes the same props as the KitchenSink, as well as a custom error handling function for custom side effects and components. It can also be styled with a custom Material-UI theme. Recommended for people who want all of the interaction support but want to add their own wrapper. 

```jsx
import React from 'react';
import { createMuiTheme } from "@material-ui/core/styles";
import { KitchenSink } from '@rainbird/sdk-react-material';

const theme = createMuiTheme({
    primary: {
        main: '#1c083b'
    },
    secondary: {
        main: '#4fbbc5'
    }
})

export const App = () => (
    <MyCustomWrapper>
        <Rainbird
            apiKey="myApiKey"
            baseURL="https://test.rainbird.ai"
            kmID="myKmID"
            subject="Bob"
            relationship="speaks"
            object=""
            onError={(err, errInfo) => {
                logErrorToCustomService(err, errInfo);
                return <CustomErrorComponent />
            }}
            theme={theme} // optional
            options={{}} // optional
        />
    </MyCustomWrapper>
)
```

##### Interaction
The Interaction component is simply the interaction with Rainbird; it won't start a session or make the initial query for you. This component is recommended for those that wish to have a custom results page/start process/query process. If you choose this component, it would be worth checking out the [@rainbird/sdk-react](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/react) to complement it. This can be styled by wrapping it in a ThemeProvider from MUI.

```jsx
import React from 'react';
import { RESPONSE_TYPE_QUESTION } from '@rainbird/sdk';
import { Rainbird } from '@rainbird/sdk-react';
import { Interaction } from '@rainbird/sdk-react-material';

const theme = createMuiTheme({
    primary: {
        main: '#1c083b'
    },
    secondary: {
        main: '#4fbbc5'
    }
})

export const App = () => (
    <MyCustomWrapper>
        <Rainbird>
            {({data, type}) => {
                if (type === RESPONSE_TYPE_QUESTION) return <Interaction data={data}>
            }}
        </Rainbird>
    </MyCustomWrapper>
)
```

##### Result
The Result component simply displays the results from a RESPONSE_TYPE_RESULT. Similarly to the Interaction component, you may want to checkout the [@rainbird/sdk-react](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/react). It can also be styled by wrapping it in a ThemeProvider from MUI.

```jsx
import React from 'react';
import { RESPONSE_TYPE_QUESTION, RESPONSE_TYPE_RESULT } from '@rainbird/sdk';
import { Rainbird } from '@rainbird/sdk-react';
import { Interaction, Result } from '@rainbird/sdk-react-material';

const theme = createMuiTheme({
    primary: {
        main: '#1c083b'
    },
    secondary: {
        main: '#4fbbc5'
    }
})

export const App = () => (
    <MyCustomWrapper>
        <Rainbird>
            {({data, type}) => {
                if (type === RESPONSE_TYPE_QUESTION) return <Interaction data={data}>
                if (type === RESPONSE_TYPE_RESULT) return <Result data={data}>
            }}
        </Rainbird>
    </MyCustomWrapper>
)
```

#### Select

The select components map to the props needed for the [FormControl](https://gitlab.com/rainbird-ai/sdk-js/-/tree/feat/4992/packages/react#formcontrol) component in the [@rainbird/sdk-react](https://gitlab.com/rainbird-ai/sdk-js/-/tree/master/packages/react) package. These are all included in the [KitchenSink](#kitchensink), [Rainbird](#rainbird) and [Interaction](#interaction) components, however you can import them directly to use in custom builds. Any props supported in the MUI docs are supported. All can be themed by wrapping them in a MUI ThemeProvider

[Certainty](#certainty) | [MultiString](#multistring) | [MultiStringAdd](#multistringadd) | [SingleDate](#singledate) | [SingleNumber](#singlenumber) | [SingleString](#singlestring) | [SingleStringAdd](#singlestringadd) | [SingleTruth](#singletruth)

**Certainty**
A MUI [slider](https://material-ui.com/api/slider/) from 1-100 to determine the certainty of a response.

**MultiString**
A multi select component with search capability. Doesn't allow a user to add custom responses. Built using MUIs [AutoComplete](https://material-ui.com/api/autocomplete/#autocomplete-api).

**MultiStringAdd**
A multi select component with search capability. Allows a user to add custom responses.Built using MUIs [AutoComplete](https://material-ui.com/api/autocomplete/#autocomplete-api).

**SingleDate**
A date [TextField](https://material-ui.com/api/text-field/#textfield-api)

**MultiDate**
A custom multi select date component. Built using MUIs [DatePicker](https://material-ui-pickers.dev/) and [AutoComplete](https://material-ui.com/api/autocomplete/#autocomplete-api).


**SingleNumber**
A number [TextField](https://material-ui.com/api/text-field/#textfield-api)

**SingleString**
A select component with search capability. Doesn't allow a user to add a custom response. Built using MUIs [AutoComplete](https://material-ui.com/api/autocomplete/#autocomplete-api).

**SingleStringAdd**
A select component with search capability. Allows a user to add a custom response.Built using MUIs [AutoComplete](https://material-ui.com/api/autocomplete/#autocomplete-api).

**SingleTruth**
Two radio buttons for 'True' and 'False'. Built using [Radio](https://material-ui.com/api/radio/), [RadioGroup](https://material-ui.com/api/radio-group/#radiogroup-api), [FormControlLabel](https://material-ui.com/api/form-control-label/#formcontrollabel-api) and [FormControl](https://material-ui.com/api/form-control/#formcontrol-api). Props can be added to them by using `radioProps`, `radioGroupProps`, `formControlLabelProps` and `formControlProps` respectively.

#### ErrorBoundary 

The error boundary is a very simple boundary that catches thrown errors bubbling up to the window. On an error, it stops rendering children and calls the onError prop. It's built into the KitchenSink and Rainbird components, so it's recommended for more custom solutions.

```jsx
export const App = () => (
    <ErrorBoundary onError={(err, errInfo) => {
        logErrorToService(err)
        return <ErrorPage error={error} info={errInfo} />
    }}>
        <CustomApp />
    </ErrorBoundary>
)
```

#### Styles

##### RBTheme
If you wanted to use our theme for your entire app, then the theme object is here. Created using createMuiTheme from MUI.
