import * as base from "../lib";

describe("base", () => {
  it("exports anything", async () => {
    expect(base).toEqual(expect.any(Object));
  });
});
