"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toTimestampAtStartOfDay = exports.maybeToString = void 0;

var maybeToString = function maybeToString(maybeValue) {
  return maybeValue ? maybeValue.toString() : maybeValue;
};

exports.maybeToString = maybeToString;

var toTimestampAtStartOfDay = function toTimestampAtStartOfDay(dateString) {
  var date = new Date(dateString);
  date.setUTCHours(0, 0, 0, 0);
  return {
    locale: date.toLocaleDateString(),
    timestamp: date.valueOf()
  };
};

exports.toTimestampAtStartOfDay = toTimestampAtStartOfDay;