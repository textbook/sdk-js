"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Rainbird = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

var _Alert = _interopRequireDefault(require("@material-ui/lab/Alert"));

var _styles = require("@material-ui/core/styles");

var _sdk = require("@rainbird/sdk");

var _sdkReact = require("@rainbird/sdk-react");

var _styles2 = require("../../styles");

var _Interaction = require("../Interaction");

var _Result = require("../Result");

var _Error = require("../Error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-next-line import/no-extraneous-dependencies
// eslint-disable-next-line import/no-extraneous-dependencies
var Rainbird = function Rainbird(_ref) {
  var kmID = _ref.kmID,
      baseURL = _ref.baseURL,
      apiKey = _ref.apiKey,
      options = _ref.options,
      _ref$theme = _ref.theme,
      theme = _ref$theme === void 0 ? _styles2.RBTheme : _ref$theme,
      subject = _ref.subject,
      object = _ref.object,
      relationship = _ref.relationship,
      onError = _ref.onError;
  return /*#__PURE__*/_react.default.createElement(_styles.ThemeProvider, {
    theme: theme
  }, /*#__PURE__*/_react.default.createElement(_Error.ErrorBoundary, {
    onError: onError
  }, /*#__PURE__*/_react.default.createElement(_sdkReact.Rainbird, {
    onLoad:
    /*#__PURE__*/
    // disabled because otherwise it complains about parentheses
    // eslint-disable-next-line react/jsx-wrap-multilines
    _react.default.createElement(_core.CircularProgress, {
      role: "status",
      "aria-label": "Loading indicator",
      "data-testid": "loading"
    }),
    onError: function onError(e) {
      return /*#__PURE__*/_react.default.createElement(_core.Snackbar, {
        open: true,
        anchorOrigin: {
          horizontal: "center",
          vertical: "top"
        }
      }, /*#__PURE__*/_react.default.createElement(_Alert.default, {
        "data-testid": "error",
        severity: "error"
      }, /*#__PURE__*/_react.default.createElement("span", {
        role: "status"
      }, e.message)));
    },
    kmID: kmID,
    baseURL: baseURL,
    apiKey: apiKey,
    options: options,
    subject: subject,
    object: object,
    relationship: relationship
  }, function (_ref2) {
    var data = _ref2.data,
        type = _ref2.type;
    if (type === _sdk.RESPONSE_TYPE_QUESTION) return /*#__PURE__*/_react.default.createElement(_Interaction.Interaction, {
      data: data
    });
    if (type === _sdk.RESPONSE_TYPE_RESULT) return /*#__PURE__*/_react.default.createElement(_Result.Result, {
      data: data
    });
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null);
  })));
};

exports.Rainbird = Rainbird;