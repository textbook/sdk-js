"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ErrorPage = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ErrorPage = function ErrorPage(_ref) {
  var onRefresh = _ref.onRefresh,
      onReport = _ref.onReport,
      error = _ref.error;
  return /*#__PURE__*/_react.default.createElement(_core.Box, {
    padding: 3,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    "data-testid": "error-title",
    variant: "h5"
  }, "An error occurred!"), /*#__PURE__*/_react.default.createElement(_core.Typography, {
    "data-testid": "error-message",
    variant: "caption"
  }, error.message), /*#__PURE__*/_react.default.createElement(_core.Box, {
    padding: 3
  }, /*#__PURE__*/_react.default.createElement(_core.Button, {
    "aria-label": "Refresh page",
    color: "primary",
    "data-testid": "error-refresh",
    onClick: onRefresh,
    title: "Refresh",
    type: "button",
    style: {
      marginRight: "10px"
    },
    variant: "outlined"
  }, "Refresh"), /*#__PURE__*/_react.default.createElement(_core.Button, {
    "aria-label": "Refresh page",
    color: "primary",
    "data-testid": "error-report",
    onClick: onReport,
    title: "Refresh",
    type: "button",
    style: {
      marginLeft: "10px"
    },
    variant: "contained"
  }, "Report")));
};

exports.ErrorPage = ErrorPage;