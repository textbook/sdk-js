"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ErrorBoundary", {
  enumerable: true,
  get: function get() {
    return _Boundary.ErrorBoundary;
  }
});
Object.defineProperty(exports, "ErrorPage", {
  enumerable: true,
  get: function get() {
    return _Page.ErrorPage;
  }
});

var _Boundary = require("./Boundary");

var _Page = require("./Page");