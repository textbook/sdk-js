"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KitchenSink = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

var _RainbirdLogo = require("./RainbirdLogo");

var _styles = require("../../styles");

var _Rainbird = require("../Rainbird");

var _Error = require("../Error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KitchenSink = function KitchenSink(_ref) {
  var apiKey = _ref.apiKey,
      baseURL = _ref.baseURL,
      kmID = _ref.kmID,
      subject = _ref.subject,
      relationship = _ref.relationship,
      object = _ref.object,
      _ref$theme = _ref.theme,
      theme = _ref$theme === void 0 ? _styles.RBTheme : _ref$theme,
      options = _ref.options;
  return /*#__PURE__*/_react.default.createElement(_core.ThemeProvider, {
    theme: theme
  }, /*#__PURE__*/_react.default.createElement(_core.Grid, {
    container: true,
    direction: "column",
    alignItems: "center"
  }, /*#__PURE__*/_react.default.createElement(_core.Grid, {
    "aria-label": "Banner",
    container: true,
    direction: "row",
    justify: "flex-end",
    alignItems: "center"
  }, /*#__PURE__*/_react.default.createElement(_RainbirdLogo.RainbirdLogo, null)), /*#__PURE__*/_react.default.createElement(_core.Grid, {
    container: true,
    direction: "column",
    justify: "center",
    alignItems: "center",
    style: {
      height: "80vh",
      minHeight: "500px"
    }
  }, /*#__PURE__*/_react.default.createElement(_core.Box, {
    p: 3
  }, /*#__PURE__*/_react.default.createElement(_core.Paper, null, /*#__PURE__*/_react.default.createElement(_core.Box, {
    width: "80vw"
  }, /*#__PURE__*/_react.default.createElement(_core.AppBar, {
    position: "relative",
    color: "primary"
  }, /*#__PURE__*/_react.default.createElement(_core.Box, {
    p: 1.5
  }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    "aria-level": "1",
    role: "heading",
    "data-testid": "query-name",
    component: "h1",
    variant: "body1"
  }, "Query:", " ".concat(subject || "?", " ").concat(relationship, " ").concat(object || "?")))), /*#__PURE__*/_react.default.createElement(_core.Box, {
    role: "main",
    p: 4,
    style: {
      overflow: "auto",
      height: "55vh",
      minHeight: "400px",
      maxHeight: "800px"
    }
  }, /*#__PURE__*/_react.default.createElement(_Rainbird.Rainbird, {
    apiKey: apiKey,
    baseURL: baseURL,
    kmID: kmID,
    subject: subject,
    relationship: relationship,
    object: object,
    theme: theme,
    options: options,
    onError:
    /* istanbul ignore next: impossible to test without adding extra props to force throw errors */
    function onError(err, errInfo) {
      return /*#__PURE__*/_react.default.createElement(_Error.ErrorPage, {
        error: err,
        errorInfo: errInfo,
        onRefresh: function onRefresh() {
          return window.location.reload();
        },
        onReport: function onReport() {
          window.open("mailto:support@rainbird-example.com?subject=".concat("Rainbird Kitchen Sink Error", "&body=", err.message, "\n\n").concat(errInfo.componentStack));
        }
      });
    }
  }))))))));
};

exports.KitchenSink = KitchenSink;