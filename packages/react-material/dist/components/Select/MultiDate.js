"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MultiDate = void 0;

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _lab = require("@material-ui/lab");

var _icons = require("@material-ui/icons");

var _pickers = require("@material-ui/pickers");

var _dayjs = _interopRequireDefault(require("@date-io/dayjs"));

var _dayjs2 = _interopRequireDefault(require("dayjs"));

var _dayjsPluginUtc = _interopRequireDefault(require("dayjs-plugin-utc"));

var _utils = require("../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

_dayjs2.default.extend(_dayjsPluginUtc.default);

var useStyles = (0, _core.makeStyles)({
  container: {
    display: "flex"
  },
  autocomplete: {
    width: "100%"
  },
  endAdornment: {
    top: "auto"
  },
  popupIndicator: {
    padding: "6px"
  }
});

var toOption = function toOption(timestamp) {
  return {
    name: new Date(timestamp).toLocaleDateString(),
    value: timestamp
  };
};

var MultiDate = function MultiDate(_ref) {
  var question = _ref.question,
      response = _ref.response,
      onChange = _ref.onChange,
      target = _ref.target,
      _ref$title = _ref.title,
      title = _ref$title === void 0 ? "Multi Date Input" : _ref$title,
      _ref$id = _ref.id,
      id = _ref$id === void 0 ? "multi-date-input" : _ref$id,
      props = _objectWithoutProperties(_ref, ["question", "response", "onChange", "target", "title", "id"]);

  var classes = useStyles();

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var knownAnswers = question.knownAnswers && question.knownAnswers[0] ? question.knownAnswers.map(function (a) {
    return a[target] ? toOption(a[target]) : null;
  }) : [];
  var knownNames = knownAnswers.map(function (ka) {
    return ka.name;
  });
  var value = response[0] && response[0][target] ? response.map(function (r) {
    return toOption(r[target]);
  }) : [];
  var values = [].concat(_toConsumableArray(knownAnswers), _toConsumableArray(value));
  var dateNames = values.map(function (v) {
    return v.name;
  });

  var renderInput = function renderInput(params) {
    return /*#__PURE__*/_react.default.createElement(_core.TextField, _extends({}, params, {
      label: "Answer",
      variant: "outlined",
      placeholder: "Answer"
    }));
  };

  var renderTags = function renderTags(v, getTagProps) {
    return v.map(function (option, index) {
      return /*#__PURE__*/_react.default.createElement(_core.Chip, _extends({
        "data-testid": "md-chip",
        label: option.name
      }, getTagProps({
        index: index
      }), {
        disabled: knownNames.includes(option.name)
      }));
    });
  };

  var renderDays = function renderDays(day, selectedDate, isInCurrentMonths, dayComponent) {
    var curr = new Date(day);

    if (dateNames.includes(curr.toLocaleDateString())) {
      return /*#__PURE__*/_react.default.createElement(_pickers.Day, {
        selected: true
      }, curr.getDate());
    }

    return dayComponent;
  };

  var handleChange = function handleChange(e, v) {
    return onChange(e, v.filter(function (o) {
      return typeof o !== "string" && !knownNames.includes(o.name);
    }));
  };

  return /*#__PURE__*/_react.default.createElement("div", _extends({}, props, {
    className: classes.container,
    id: id,
    "aria-label": title,
    title: title
  }), /*#__PURE__*/_react.default.createElement(_lab.Autocomplete, {
    className: classes.autocomplete,
    classes: {
      endAdornment: classes.endAdornment,
      popupIndicator: classes.popupIndicator
    },
    options: [],
    value: values,
    multiple: true,
    getOptionLabel: function getOptionLabel(opt) {
      return opt.name;
    },
    onChange: handleChange,
    renderInput: renderInput,
    renderTags: renderTags,
    onOpen: function onOpen() {
      return setOpen(true);
    },
    popupIcon: /*#__PURE__*/_react.default.createElement(_icons.Event, {
      fontSize: "small"
    })
  }), /*#__PURE__*/_react.default.createElement(_pickers.MuiPickersUtilsProvider, {
    utils: _dayjs.default
  }, /*#__PURE__*/_react.default.createElement(_pickers.KeyboardDatePicker, {
    minDate: new Date("1000/01/01"),
    maxDate: new Date("5000/12/31"),
    open: open,
    onOpen: function onOpen() {
      return setOpen(true);
    },
    onClose: function onClose() {
      return setOpen(false);
    },
    TextFieldComponent: function TextFieldComponent() {
      return null;
    },
    format: "DD/MM/YYYY",
    placeholder: "dd/mm/yyyy",
    onChange: function onChange(d) {
      var localeWithTimestamp = (0, _utils.toTimestampAtStartOfDay)(d);
      var newValue = {
        name: localeWithTimestamp.locale,
        value: localeWithTimestamp.timestamp
      };

      if (dateNames.includes(newValue.name)) {
        return handleChange(null, value);
      }

      return handleChange(null, [].concat(_toConsumableArray(value), [newValue]));
    },
    renderDay: renderDays
  })));
};

exports.MultiDate = MultiDate;