"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SingleDate = void 0;

var _react = _interopRequireDefault(require("react"));

var _pickers = require("@material-ui/pickers");

var _dayjs = _interopRequireDefault(require("@date-io/dayjs"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var SingleDate = function SingleDate(_ref) {
  var onChange = _ref.onChange,
      value = _ref.value,
      _ref$title = _ref.title,
      title = _ref$title === void 0 ? "Date input" : _ref$title,
      _ref$id = _ref.id,
      id = _ref$id === void 0 ? "dateInput" : _ref$id,
      _ref$ariaLabel = _ref.ariaLabel,
      ariaLabel = _ref$ariaLabel === void 0 ? "Date input" : _ref$ariaLabel,
      props = _objectWithoutProperties(_ref, ["onChange", "value", "title", "id", "ariaLabel"]);

  return (
    /*#__PURE__*/
    // label is associated with control but linting falls over
    // eslint-disable-next-line jsx-a11y/label-has-associated-control
    _react.default.createElement("label", {
      htmlFor: id,
      "aria-label": ariaLabel
    }, /*#__PURE__*/_react.default.createElement(_pickers.MuiPickersUtilsProvider, {
      utils: _dayjs.default
    }, /*#__PURE__*/_react.default.createElement(_pickers.KeyboardDatePicker, _extends({
      title: title,
      id: id,
      "aria-label": ariaLabel,
      value: value,
      fullWidth: true,
      onChange: onChange
    }, props))))
  );
};

exports.SingleDate = SingleDate;