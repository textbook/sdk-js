"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SingleTruth = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var SingleTruth = function SingleTruth(_ref) {
  var value = _ref.value,
      onChange = _ref.onChange,
      _ref$formContolProps = _ref.formContolProps,
      formContolProps = _ref$formContolProps === void 0 ? {} : _ref$formContolProps,
      _ref$radioControlProp = _ref.radioControlProps,
      radioControlProps = _ref$radioControlProp === void 0 ? {} : _ref$radioControlProp,
      _ref$formControlLabel = _ref.formControlLabelProps,
      formControlLabelProps = _ref$formControlLabel === void 0 ? {} : _ref$formControlLabel,
      _ref$radioProps = _ref.radioProps,
      radioProps = _ref$radioProps === void 0 ? {} : _ref$radioProps,
      props = _objectWithoutProperties(_ref, ["value", "onChange", "formContolProps", "radioControlProps", "formControlLabelProps", "radioProps"]);

  return /*#__PURE__*/_react.default.createElement(_core.FormControl, _extends({
    component: "fieldset"
  }, formContolProps, props), /*#__PURE__*/_react.default.createElement(_core.RadioGroup, _extends({
    title: "Truth select",
    "aria-label": "Truth select",
    name: "Truth select",
    value: value,
    onChange: onChange
  }, radioControlProps), /*#__PURE__*/_react.default.createElement(_core.FormControlLabel, _extends({
    value: "true",
    control: /*#__PURE__*/_react.default.createElement(_core.Radio, radioProps),
    label: "True"
  }, formControlLabelProps)), /*#__PURE__*/_react.default.createElement(_core.FormControlLabel, _extends({
    value: "false",
    control: /*#__PURE__*/_react.default.createElement(_core.Radio, radioProps),
    label: "False"
  }, formControlLabelProps))));
};

exports.SingleTruth = SingleTruth;