"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SingleStringAdd = void 0;

var _react = _interopRequireDefault(require("react"));

var _lab = require("@material-ui/lab");

var _InputBase = require("./InputBase");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var filter = (0, _lab.createFilterOptions)();

var SingleStringAdd = function SingleStringAdd(_ref) {
  var options = _ref.options,
      value = _ref.value,
      onChange = _ref.onChange,
      props = _objectWithoutProperties(_ref, ["options", "value", "onChange"]);

  return /*#__PURE__*/_react.default.createElement(_lab.Autocomplete, _extends({
    title: "Single string select",
    "aria-label": "Single string select",
    value: value,
    options: options || [],
    getOptionLabel: function getOptionLabel(option) {
      // Value selected with enter, right from the input
      if (typeof option === "string") {
        return option;
      } // // Add "xxx" option created dynamically


      if (option.overrideTitle) {
        return option.overrideTitle;
      } // Regular option


      return option.name;
    },
    onChange: onChange,
    filterOptions: function filterOptions(opts, params) {
      var filtered = filter(opts, params);

      if (params.inputValue !== "") {
        filtered.push({
          value: params.inputValue,
          name: "Add \"".concat(params.inputValue, "\""),
          type: "string",
          overrideTitle: params.inputValue
        });
      }

      return filtered;
    },
    freeSolo: true,
    renderOption: function renderOption(option) {
      return option.name;
    },
    renderInput: function renderInput(params) {
      return /*#__PURE__*/_react.default.createElement(_InputBase.InputBase, params);
    }
  }, props));
};

exports.SingleStringAdd = SingleStringAdd;