"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MultiNumberAdd = exports.MultiStringAdd = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

var _lab = require("@material-ui/lab");

var _utils = require("../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var filter = (0, _lab.createFilterOptions)();

var MultiSelectAdd = function MultiSelectAdd(_ref) {
  var title = _ref.title,
      onChange = _ref.onChange,
      question = _ref.question,
      response = _ref.response,
      target = _ref.target,
      props = _objectWithoutProperties(_ref, ["title", "onChange", "question", "response", "target"]);

  // ToDo refactor this component and MultiSelect to share more code.
  var knownAnswers = question.knownAnswers && question.knownAnswers[0] ? question.knownAnswers.map(function (a) {
    return (0, _utils.maybeToString)(a[target]);
  }) : [];
  var opts = question.concepts && question.concepts.length > 0 ? question.concepts.map(function (c) {
    return {
      name: (0, _utils.maybeToString)(c.value),
      value: (0, _utils.maybeToString)(c.value)
    };
  }) : [];
  var value = response[0] && response[0][target] ? response.map(function (r) {
    return {
      name: (0, _utils.maybeToString)(r[target]),
      value: (0, _utils.maybeToString)(r[target])
    };
  }) : [];

  var handleChange = function handleChange(e, v) {
    onChange(e, v.filter(function (o) {
      return typeof o !== "string" && !knownAnswers.includes(o.value);
    }));
  };

  var renderInput = function renderInput(params) {
    return /*#__PURE__*/_react.default.createElement(_core.TextField, _extends({}, params, {
      label: "Answer",
      variant: "outlined",
      placeholder: "Answer"
    }));
  };

  var getTags = function getTags(tagValue, getTagProps) {
    return tagValue.map(function (option, index) {
      return /*#__PURE__*/_react.default.createElement(_core.Chip, _extends({
        label: option.name
      }, getTagProps({
        index: index
      }), {
        disabled: knownAnswers.includes(option.name)
      }));
    });
  };

  var filterOptions = function filterOptions(options, params) {
    var filtered = filter(options, params);

    if (params.inputValue !== "") {
      filtered.push({
        known: false,
        overrideTitle: "Add \"".concat(params.inputValue, "\""),
        value: params.inputValue
      });
    }

    return filtered;
  };

  return /*#__PURE__*/_react.default.createElement(_lab.Autocomplete, _extends({
    title: title,
    "aria-label": title,
    multiple: true,
    value: [].concat(_toConsumableArray(opts.filter(function (o) {
      return knownAnswers.includes(o.value);
    })), _toConsumableArray(value)),
    options: opts,
    getOptionLabel: function getOptionLabel(option) {
      // Value selected with enter, right from the input
      if (typeof option === "string") {
        return option;
      } // // Add "xxx" option created dynamically


      if (option.overrideTitle) {
        return option.overrideTitle;
      } // Regular option


      return option.name;
    },
    getOptionSelected: function getOptionSelected(opt, v) {
      return opt.name === v.name;
    },
    disableCloseOnSelect: true,
    onChange: handleChange,
    filterOptions: filterOptions,
    renderTags: getTags,
    renderInput: renderInput
  }, props));
};

var MultiStringAdd = function MultiStringAdd(_ref2) {
  var onChange = _ref2.onChange,
      question = _ref2.question,
      response = _ref2.response,
      target = _ref2.target,
      props = _objectWithoutProperties(_ref2, ["onChange", "question", "response", "target"]);

  return MultiSelectAdd(_objectSpread({
    title: "Multi string select",
    onChange: onChange,
    question: question,
    response: response,
    target: target
  }, props));
};

exports.MultiStringAdd = MultiStringAdd;

var MultiNumberAdd = function MultiNumberAdd(_ref3) {
  var onChange = _ref3.onChange,
      question = _ref3.question,
      response = _ref3.response,
      target = _ref3.target,
      props = _objectWithoutProperties(_ref3, ["onChange", "question", "response", "target"]);

  return MultiSelectAdd(_objectSpread({
    title: "Multi number select",
    onChange: onChange,
    question: question,
    response: response,
    target: target
  }, props));
};

exports.MultiNumberAdd = MultiNumberAdd;