"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Interaction = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactMarkdown = _interopRequireDefault(require("react-markdown"));

var _core = require("@material-ui/core");

var _lab = require("@material-ui/lab");

var _sdkReact = require("@rainbird/sdk-react");

var _dayjs = _interopRequireDefault(require("dayjs"));

var _dayjsPluginUtc = _interopRequireDefault(require("dayjs-plugin-utc"));

var _Select = require("../Select");

var _utils = require("../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

_dayjs.default.extend(_dayjsPluginUtc.default);

var Interaction = function Interaction(_ref) {
  var data = _ref.data,
      error = _ref.error;
  var question = data.question,
      extraQuestions = data.extraQuestions;
  var respond = (0, _sdkReact.useResponse)();
  var undo = (0, _sdkReact.useUndo)();
  var initialState = data.question ? [[{
    cf: 100,
    object: question.object,
    relationship: question.relationship,
    subject: question.subject
  }]].concat(_toConsumableArray(extraQuestions.map(function (_ref2) {
    var subject = _ref2.subject,
        relationship = _ref2.relationship,
        object = _ref2.object;
    return [{
      subject: subject,
      relationship: relationship,
      object: object,
      cf: 100
    }];
  }))) : undefined;

  var _useState = (0, _react.useState)(initialState),
      _useState2 = _slicedToArray(_useState, 2),
      response = _useState2[0],
      setResponse = _useState2[1];

  var getQuestionTarget = function getQuestionTarget(q) {
    var qType = q.type && q.type.toLowerCase();
    if (qType === "first form") return "answer";
    return qType === "second form object" ? "object" : "subject";
  };

  var submitDisabled = response && response.map(function (group, i) {
    return group.map(function (r) {
      var q = i > 0 ? extraQuestions[i - 1] : question;
      var target = r[getQuestionTarget(q)];
      var knownAnswers = Array.isArray(q.knownAnswers) && q.knownAnswers.length;
      if (target === undefined && q.allowUnknown || knownAnswers) return false;
      if (target === undefined) return true;
      return false;
    }).includes(true);
  }).includes(true);

  var submit = function submit(e) {
    e.preventDefault();
    if (submitDisabled) return;
    var res = flattenResponse().map(function (r) {
      var item = r;
      var unanswered = !r.subject && typeof r.subject !== "boolean" || !r.object && typeof r.object !== "boolean";
      if (unanswered) item.unanswered = true;
      return item;
    });
    respond(res);
  };

  var handleSkip = function handleSkip() {
    return respond([{
      cf: 100,
      object: question.object || "",
      relationship: question.relationship,
      subject: question.subject || "",
      unanswered: true
    }].concat(_toConsumableArray(extraQuestions.map(function (_ref3) {
      var subject = _ref3.subject,
          relationship = _ref3.relationship,
          object = _ref3.object;
      return [{
        cf: 100,
        object: object,
        relationship: relationship,
        subject: subject,
        unanswered: true
      }];
    }))));
  };

  var flattenResponse = function flattenResponse() {
    return response.reduce(function (result, g) {
      return result.concat(g);
    }, []);
  }; // Rebuild res and update the target question group


  var _handleResChange = function handleResChange(group, i) {
    return setResponse(function (prevRes) {
      return prevRes.map(function (prevGroup, j) {
        return i === j ? group : prevGroup;
      });
    });
  };

  return /*#__PURE__*/_react.default.createElement("form", {
    style: {
      alignItems: "center",
      display: "flex",
      height: "100%",
      justifyContent: "center"
    },
    onSubmit: submit
  }, error && /*#__PURE__*/_react.default.createElement(_core.Snackbar, {
    open: true,
    anchorOrigin: {
      horizontal: "center",
      vertical: "top"
    }
  }, /*#__PURE__*/_react.default.createElement(_lab.Alert, {
    "data-testid": "error",
    severity: "error"
  }, error.map(function (e) {
    return /*#__PURE__*/_react.default.createElement(_core.Typography, {
      key: e
    }, e);
  }))), data && data.question && /*#__PURE__*/_react.default.createElement(_core.Grid, {
    container: true,
    direction: "column",
    justify: "space-between",
    style: {
      height: "100%"
    },
    wrap: "nowrap"
  }, /*#__PURE__*/_react.default.createElement(BoxContainer, {
    question: question,
    response: response[0],
    handleResChange: function handleResChange(g) {
      return _handleResChange(g, 0);
    },
    target: getQuestionTarget(question)
  }), extraQuestions && extraQuestions.map(function (q, i) {
    return /*#__PURE__*/_react.default.createElement(BoxContainer, {
      key: "extraQuestionGroup-".concat(i),
      question: q,
      response: response[i + 1],
      handleResChange: function handleResChange(g) {
        return _handleResChange(g, i + 1);
      },
      target: getQuestionTarget(question)
    });
  }), /*#__PURE__*/_react.default.createElement(_core.Grid, {
    container: true,
    direction: "row",
    justify: "space-between"
  }, /*#__PURE__*/_react.default.createElement(_core.Button, {
    "aria-label": "Back to previous question",
    color: "primary",
    "data-testid": "undo-response",
    onClick: undo,
    title: "Back to previous question",
    type: "button",
    variant: "outlined"
  }, "Back ", /*#__PURE__*/_react.default.createElement("span", {
    style: {
      display: "none"
    }
  }, "to previous question")), /*#__PURE__*/_react.default.createElement(_core.Grid, null, question.allowUnknown && /*#__PURE__*/_react.default.createElement(_core.Button, {
    color: "primary",
    "data-testid": "skip-question",
    onClick: handleSkip,
    type: "button",
    variant: "outlined"
  }, "Skip"), /*#__PURE__*/_react.default.createElement(_core.Button, {
    "aria-disabled": submitDisabled,
    "aria-label": "Submit response",
    color: "primary",
    "data-testid": "submit-response",
    disabled: submitDisabled,
    style: {
      marginLeft: "10px"
    },
    title: "Submit response",
    type: "submit",
    variant: "contained"
  }, "Submit ", /*#__PURE__*/_react.default.createElement("span", {
    style: {
      display: "none"
    }
  }, "response"))))));
};

exports.Interaction = Interaction;

var BoxContainer = function BoxContainer(_ref4) {
  var handleResChange = _ref4.handleResChange,
      question = _ref4.question,
      response = _ref4.response,
      target = _ref4.target;

  // Replace item in the response array on change
  var handleSimpleChange = function handleSimpleChange(_, newValue) {
    var newResponse = response[0];
    newResponse[target] = newValue && newValue.value || undefined;
    return handleResChange([newResponse]);
  }; // Replace multi-item in the response array on change


  var handleMultiChange = function handleMultiChange(_, v) {
    if (v.length === 0) {
      var _newResponse = response[0];
      _newResponse = [{
        subject: question.subject,
        relationship: question.relationship,
        object: question.object,
        cf: 100
      }];
      return handleResChange(_newResponse);
    }

    var uniqueItems = Array.from(new Set(v.map(function (i) {
      return i.value;
    }))).map(function (name) {
      return v.find(function (i) {
        return i.value === name;
      });
    });
    var newResponse = uniqueItems.map(function (inst) {
      return {
        object: question.object || inst.value,
        relationship: question.relationship,
        subject: question.subject || inst.value,
        cf: response[0] && response[0].cf
      };
    });
    return handleResChange(newResponse);
  };

  var handleSingleTruthOnChange = function handleSingleTruthOnChange(change) {
    var value = change.target.value;
    var valueIsTrue = value === "true";
    var answer = valueIsTrue ? "yes" : "no"; // If the answer is first form, we want to reply yes/no to answer

    var isFirstForm = question && question.type && question.type.toLowerCase() === "first form";
    var newResponse = response[0];
    newResponse[target] = isFirstForm ? answer : valueIsTrue;
    return handleResChange([newResponse]);
  };

  return /*#__PURE__*/_react.default.createElement(_core.Box, null, /*#__PURE__*/_react.default.createElement(_core.Typography, {
    component: "h2",
    "data-testid": "prompt",
    id: "prompt",
    variant: "body1"
  }, /*#__PURE__*/_react.default.createElement(_reactMarkdown.default, null, question && question.prompt)), /*#__PURE__*/_react.default.createElement(_core.Grid, {
    container: true,
    direction: "column",
    justify: "space-between"
  }, /*#__PURE__*/_react.default.createElement(_core.Box, {
    paddingTop: 2,
    paddingBottom: 2
  }, /*#__PURE__*/_react.default.createElement(_sdkReact.FormControl, {
    data: question,
    singleStringAdd: question.concepts && question.concepts.length > 0 ? /*#__PURE__*/_react.default.createElement(_Select.SingleStringAdd, {
      "data-testid": "single-string-add",
      value: response[0][target],
      options: question.concepts,
      onChange: handleSimpleChange
    }) : /*#__PURE__*/_react.default.createElement(_Select.InputBase, {
      fullWidth: true,
      onChange: function onChange(e) {
        return handleSimpleChange("", e.target);
      },
      value: response[0][target],
      "data-testid": "single-string-add-noInsts"
    }),
    singleString: /*#__PURE__*/_react.default.createElement(_Select.SingleString, {
      "data-testid": "single-string",
      value: response[0][target],
      options: question.concepts,
      onChange: handleSimpleChange
    }),
    multiStringAdd: /*#__PURE__*/_react.default.createElement(_Select.MultiStringAdd, {
      "data-testid": "multi-string-add",
      question: question,
      onChange: handleMultiChange,
      response: response,
      target: target
    }),
    multiString: /*#__PURE__*/_react.default.createElement(_Select.MultiString, {
      "data-testid": "multi-string",
      question: question,
      onChange: handleMultiChange,
      response: response,
      target: target
    }),
    singleDate: /*#__PURE__*/_react.default.createElement(_Select.SingleDate, {
      minDate: new Date("1000/01/01"),
      maxDate: new Date("5000/12/31"),
      "data-testid": "single-date",
      format: "DD/MM/YYYY",
      initialFocusedDate: new Date(),
      value: response[0][target] ? new Date(response[0][target]) : null,
      placeholder: "dd/mm/yyyy",
      onChange: function onChange(date) {
        if (!date) return;
        var newResponse = response[0];
        newResponse[target] = (0, _utils.toTimestampAtStartOfDay)(date).timestamp;
        return handleResChange([newResponse]);
      }
    }),
    multiDate: /*#__PURE__*/_react.default.createElement(_Select.MultiDate, {
      "data-testid": "multi-date",
      question: question,
      response: response,
      onChange: handleMultiChange,
      target: target
    }),
    singleNumber: /*#__PURE__*/_react.default.createElement(_Select.SingleNumber, {
      "data-testid": "single-number",
      value: response[0][target],
      onChange: function onChange(e) {
        var newResponse = response[0];
        newResponse[target] = e.target.value;
        return handleResChange([newResponse]);
      }
    }),
    multiNumber: /*#__PURE__*/_react.default.createElement(_Select.MultiNumber, {
      "data-testid": "multi-number",
      question: question,
      onChange: handleMultiChange,
      response: response,
      target: target
    }),
    multiNumberAdd: /*#__PURE__*/_react.default.createElement(_Select.MultiNumberAdd, {
      "data-testid": "multi-number-add",
      question: question,
      onChange: handleMultiChange,
      response: response,
      target: target
    }),
    singleTruth: /*#__PURE__*/_react.default.createElement(_Select.SingleTruth, {
      "data-testid": "single-truth",
      onChange: handleSingleTruthOnChange,
      value: response[0][target]
    }),
    certaintySelect: /*#__PURE__*/_react.default.createElement(_core.Box, {
      paddingTop: 3
    }, /*#__PURE__*/_react.default.createElement(_Select.Certainty, {
      "data-testid": "certainty",
      value: response[0].cf,
      onChange: function onChange(_, v) {
        var newResponse = response[0];
        newResponse.cf = v;
        return handleResChange([newResponse]);
      }
    })),
    fallback: /*#__PURE__*/_react.default.createElement("p", null, "something went wrong")
  }))));
};