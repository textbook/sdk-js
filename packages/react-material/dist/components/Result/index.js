"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Result = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Result = function Result(_ref) {
  var data = _ref.data;
  return /*#__PURE__*/_react.default.createElement(_core.Grid, {
    role: "main",
    container: true,
    direction: "column",
    justify: "flex-start"
  }, data.length ? /*#__PURE__*/_react.default.createElement(_core.Typography, {
    "aria-level": "2",
    role: "heading",
    variant: "h5"
  }, "Your results:") : /*#__PURE__*/_react.default.createElement(_core.Typography, {
    "data-testid": "error"
  }, "Sorry I've been unable to find an answer to your question!"), /*#__PURE__*/_react.default.createElement("ul", null, data.map(function (d) {
    var result = "".concat(d.subject, " ").concat(d.relationship, " ").concat(d.object, " - ").concat(d.certainty, "%");
    return /*#__PURE__*/_react.default.createElement("li", {
      key: result
    }, /*#__PURE__*/_react.default.createElement(_core.Typography, {
      "aria-label": "Results",
      "data-testid": "result"
    }, result));
  })));
};

exports.Result = Result;