"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "RBTheme", {
  enumerable: true,
  get: function get() {
    return _theme.RBTheme;
  }
});

var _theme = require("./theme");