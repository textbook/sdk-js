"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RBTheme = void 0;

var _styles = require("@material-ui/core/styles");

// Testing this would only test 3rd party software

/* istanbul ignore next */
var RBTheme = (0, _styles.createMuiTheme)({
  palette: {
    primary: {
      main: "#6200EE"
    },
    secondary: {
      main: "#4fbbc5"
    }
  }
});
exports.RBTheme = RBTheme;