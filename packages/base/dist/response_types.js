"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parse = parse;
exports.RESPONSE_TYPE_UNKNOWN = exports.RESPONSE_TYPE_RESULT = exports.RESPONSE_TYPE_QUESTION = void 0;
var RESPONSE_TYPE_QUESTION = Symbol("RESPONSE_TYPE_QUESTION");
exports.RESPONSE_TYPE_QUESTION = RESPONSE_TYPE_QUESTION;
var RESPONSE_TYPE_RESULT = Symbol("RESPONSE_TYPE_RESULT");
exports.RESPONSE_TYPE_RESULT = RESPONSE_TYPE_RESULT;
var RESPONSE_TYPE_UNKNOWN = Symbol("RESPONSE_TYPE_UNKNOWN");
exports.RESPONSE_TYPE_UNKNOWN = RESPONSE_TYPE_UNKNOWN;

function parse(data) {
  if (data.question !== undefined) {
    return {
      type: RESPONSE_TYPE_QUESTION,
      data: {
        question: data.question,
        extraQuestions: data.extraQuestions || []
      }
    };
  }

  if (data.result !== undefined) {
    return {
      type: RESPONSE_TYPE_RESULT,
      data: data.result
    };
  }

  return {
    type: RESPONSE_TYPE_UNKNOWN,
    data: data
  };
}