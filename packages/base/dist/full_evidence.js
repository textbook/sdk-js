"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fullEvidence = void 0;

var _evidence = require("./evidence");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var factMap = new Map();

var buildFacts = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(baseURL, sessionID, factID, options) {
    var factEvidence, promisedFacts;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return (0, _evidence.evidence)(baseURL, sessionID, factID, options);

          case 2:
            factEvidence = _context2.sent;

            if (factEvidence) {
              _context2.next = 5;
              break;
            }

            return _context2.abrupt("return", []);

          case 5:
            if (!factMap.has(factEvidence.factID)) {
              _context2.next = 7;
              break;
            }

            return _context2.abrupt("return", []);

          case 7:
            factMap.set(factEvidence.factID, factEvidence);

            if (factEvidence.rule) {
              _context2.next = 10;
              break;
            }

            return _context2.abrupt("return", []);

          case 10:
            promisedFacts = factEvidence.rule.conditions.map( /*#__PURE__*/function () {
              var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_ref2) {
                var fid;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        fid = _ref2.factID;

                        if (fid) {
                          _context.next = 3;
                          break;
                        }

                        return _context.abrupt("return", []);

                      case 3:
                        return _context.abrupt("return", buildFacts(baseURL, sessionID, fid, options));

                      case 4:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));

              return function (_x5) {
                return _ref3.apply(this, arguments);
              };
            }());
            _context2.next = 13;
            return Promise.all(promisedFacts);

          case 13:
            return _context2.abrupt("return", promisedFacts);

          case 14:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function buildFacts(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}();

var fullEvidence = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(baseURL, sessionID, factID) {
    var options,
        _args3 = arguments;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            options = _args3.length > 3 && _args3[3] !== undefined ? _args3[3] : {};
            _context3.next = 3;
            return buildFacts(baseURL, sessionID, factID, options);

          case 3:
            return _context3.abrupt("return", Array.from(factMap.values()));

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function fullEvidence(_x6, _x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}();

exports.fullEvidence = fullEvidence;