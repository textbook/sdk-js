"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.response = response;

var _response_types = require("./response_types");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// Returns question(s) or result
function response(_x, _x2, _x3) {
  return _response.apply(this, arguments);
}

function _response() {
  _response = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(baseURL, sessionID, answers) {
    var options,
        _global,
        fetch,
        engine,
        headers,
        ret,
        _args = arguments;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            options = _args.length > 3 && _args[3] !== undefined ? _args[3] : {
              engine: undefined,
              context: undefined
            };
            _global = global, fetch = _global.fetch;
            engine = options.engine;
            headers = _objectSpread({
              "Content-Type": "application/json"
            }, engine && {
              "x-rainbird-engine": engine
            });
            _context.next = 6;
            return fetch("".concat(baseURL, "/").concat(sessionID, "/response"), {
              method: "POST",
              headers: headers,
              body: JSON.stringify({
                answers: answers
              })
            });

          case 6:
            ret = _context.sent;

            if (ret.ok) {
              _context.next = 9;
              break;
            }

            throw new Error("API error code: ".concat(ret.status));

          case 9:
            _context.t0 = _response_types.parse;
            _context.next = 12;
            return ret.json();

          case 12:
            _context.t1 = _context.sent;
            return _context.abrupt("return", (0, _context.t0)(_context.t1));

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _response.apply(this, arguments);
}