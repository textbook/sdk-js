"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.undo = undo;

var _response_types = require("./response_types");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function undo(_x, _x2) {
  return _undo.apply(this, arguments);
}

function _undo() {
  _undo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(baseURL, sessionID) {
    var data,
        options,
        _global,
        fetch,
        engine,
        headers,
        response,
        _args = arguments;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            data = _args.length > 2 && _args[2] !== undefined ? _args[2] : {};
            options = _args.length > 3 && _args[3] !== undefined ? _args[3] : {
              engine: undefined,
              context: undefined
            };
            _global = global, fetch = _global.fetch;
            engine = options.engine;
            headers = _objectSpread({
              "Content-Type": "application/json"
            }, engine && {
              "x-rainbird-engine": engine
            });
            _context.next = 7;
            return fetch("".concat(baseURL, "/").concat(sessionID, "/undo"), {
              method: "POST",
              headers: headers,
              body: JSON.stringify({
                data: data
              })
            });

          case 7:
            response = _context.sent;

            if (response.ok) {
              _context.next = 10;
              break;
            }

            throw new Error("API error code: ".concat(response.status));

          case 10:
            _context.t0 = _response_types.parse;
            _context.next = 13;
            return response.json();

          case 13:
            _context.t1 = _context.sent;
            return _context.abrupt("return", (0, _context.t0)(_context.t1));

          case 15:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _undo.apply(this, arguments);
}