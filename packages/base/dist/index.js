"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "RESPONSE_TYPE_QUESTION", {
  enumerable: true,
  get: function get() {
    return _response_types.RESPONSE_TYPE_QUESTION;
  }
});
Object.defineProperty(exports, "RESPONSE_TYPE_RESULT", {
  enumerable: true,
  get: function get() {
    return _response_types.RESPONSE_TYPE_RESULT;
  }
});
Object.defineProperty(exports, "RESPONSE_TYPE_UNKNOWN", {
  enumerable: true,
  get: function get() {
    return _response_types.RESPONSE_TYPE_UNKNOWN;
  }
});
Object.defineProperty(exports, "evidence", {
  enumerable: true,
  get: function get() {
    return _evidence.evidence;
  }
});
Object.defineProperty(exports, "fullEvidence", {
  enumerable: true,
  get: function get() {
    return _full_evidence.fullEvidence;
  }
});
Object.defineProperty(exports, "inject", {
  enumerable: true,
  get: function get() {
    return _inject.inject;
  }
});
Object.defineProperty(exports, "query", {
  enumerable: true,
  get: function get() {
    return _query.query;
  }
});
Object.defineProperty(exports, "response", {
  enumerable: true,
  get: function get() {
    return _response.response;
  }
});
Object.defineProperty(exports, "start", {
  enumerable: true,
  get: function get() {
    return _start.start;
  }
});
Object.defineProperty(exports, "undo", {
  enumerable: true,
  get: function get() {
    return _undo.undo;
  }
});
Object.defineProperty(exports, "interactionLog", {
  enumerable: true,
  get: function get() {
    return _interaction_log.interactionLog;
  }
});
exports.ENGINE_BETA = exports.ENGINE_CORE = exports.URL_ENTERPRISE = exports.URL_COMMUNITY = void 0;

require("core-js/stable");

require("regenerator-runtime/runtime");

require("./polyfill");

var _response_types = require("./response_types");

var _evidence = require("./evidence");

var _full_evidence = require("./full_evidence");

var _inject = require("./inject");

var _query = require("./query");

var _response = require("./response");

var _start = require("./start");

var _undo = require("./undo");

var _interaction_log = require("./interaction_log");

var URL_COMMUNITY = "https://api.rainbird.ai";
exports.URL_COMMUNITY = URL_COMMUNITY;
var URL_ENTERPRISE = "https://enterprise-api.rainbird.ai";
exports.URL_ENTERPRISE = URL_ENTERPRISE;
var ENGINE_CORE = "Core";
exports.ENGINE_CORE = ENGINE_CORE;
var ENGINE_BETA = "Experimental (Beta)";
exports.ENGINE_BETA = ENGINE_BETA;