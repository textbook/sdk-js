"use strict";

var _abab = _interopRequireDefault(require("abab"));

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* istanbul ignore file */

/* eslint import/no-extraneous-dependencies: 0 */
global.btoa = global.btoa || _abab.default.btoa;
global.atob = global.atob || _abab.default.atob;
global.fetch = global.fetch || _nodeFetch.default;