"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.start = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var start = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(baseURL, apiKey, kmID) {
    var options,
        _global,
        fetch,
        btoa,
        encodeURIComponent,
        engine,
        context,
        headers,
        url,
        response,
        data,
        _args = arguments;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            options = _args.length > 3 && _args[3] !== undefined ? _args[3] : {
              engine: undefined,
              context: undefined
            };
            _global = global, fetch = _global.fetch, btoa = _global.btoa, encodeURIComponent = _global.encodeURIComponent;
            engine = options.engine, context = options.context;
            headers = _objectSpread({
              "Content-Type": "application/json",
              Authorization: "Basic ".concat(btoa("".concat(apiKey, ":")))
            }, engine && {
              "x-rainbird-engine": engine
            });
            url = "".concat(baseURL, "/start/").concat(kmID).concat(context ? "?".concat(encodeURIComponent("contextId"), "=").concat(encodeURIComponent(context)) : "");
            _context.next = 7;
            return fetch(url, {
              headers: headers
            });

          case 7:
            response = _context.sent;

            if (response.ok) {
              _context.next = 10;
              break;
            }

            throw new Error("API error code: ".concat(response.status));

          case 10:
            _context.next = 12;
            return response.json();

          case 12:
            data = _context.sent;
            return _context.abrupt("return", {
              sessionID: data.id,
              kmVersionID: data.kmVersion.id
            });

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function start(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.start = start;