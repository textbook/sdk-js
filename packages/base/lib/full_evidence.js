import { evidence } from "./evidence";

const factMap = new Map();

const buildFacts = async (baseURL, sessionID, factID, options) => {
  const factEvidence = await evidence(baseURL, sessionID, factID, options);
  if (!factEvidence) return [];
  if (factMap.has(factEvidence.factID)) return [];

  factMap.set(factEvidence.factID, factEvidence);

  if (!factEvidence.rule) return [];

  const promisedFacts = factEvidence.rule.conditions.map(
    async ({ factID: fid }) => {
      if (!fid) return [];
      return buildFacts(baseURL, sessionID, fid, options);
    },
  );

  await Promise.all(promisedFacts);
  return promisedFacts;
};

export const fullEvidence = async (
  baseURL,
  sessionID,
  factID,
  options = {},
) => {
  await buildFacts(baseURL, sessionID, factID, options);
  return Array.from(factMap.values());
};
