import { parse } from "./response_types";
import {
  Options,
  ConceptInstance,
  ResultResponse,
  QuestionResponse,
} from "./domain";

// Returns question(s) or result
export async function query(
  baseURL: string,
  sessionID: string,
  subject: ConceptInstance | undefined = undefined,
  relationship: string,
  object: ConceptInstance | undefined = undefined,
  options: Options = { engine: undefined, context: undefined },
): Promise<ResultResponse | QuestionResponse> {
  const { fetch } = global;
  const { engine } = options;

  const headers = {
    "Content-Type": "application/json",
    ...(engine && { "x-rainbird-engine": engine }),
  };

  const body = {
    relationship,
    ...(subject !== undefined && { subject }),
    ...(object !== undefined && { object }),
  };

  const response = await fetch(`${baseURL}/${sessionID}/query`, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
  });
  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  return parse(await response.json());
}
