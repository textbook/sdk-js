/* istanbul ignore file */
/* eslint import/no-extraneous-dependencies: 0 */

import abab from "abab";
import fetch from "node-fetch";

global.btoa = global.btoa || abab.btoa;
global.atob = global.atob || abab.atob;
global.fetch = global.fetch || fetch;
