import { parse } from "./response_types";
import { Options, ResultResponse, QuestionResponse } from "./domain";

export async function undo(
  baseURL: string,
  sessionID: string,
  data: any = {},
  options: Options = { engine: undefined, context: undefined },
): Promise<ResultResponse | QuestionResponse> {
  const { fetch } = global;
  const { engine } = options;

  const headers = {
    "Content-Type": "application/json",
    ...(engine && { "x-rainbird-engine": engine }),
  };

  const response = await fetch(`${baseURL}/${sessionID}/undo`, {
    method: "POST",
    headers,
    body: JSON.stringify({ data }),
  });
  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  return parse(await response.json());
}
