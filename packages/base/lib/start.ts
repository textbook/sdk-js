interface StartResponse {
  sessionID: string;
  kmVersionID: string;
}

interface Options {
  engine?: string;
  context?: string;
}

export const start = async (
  baseURL: string,
  apiKey: string,
  kmID: string,
  options: Options = { engine: undefined, context: undefined },
): Promise<StartResponse> => {
  const { fetch, btoa, encodeURIComponent } = global;
  const { engine, context } = options;

  const headers = {
    "Content-Type": "application/json",
    Authorization: `Basic ${btoa(`${apiKey}:`)}`,
    ...(engine && { "x-rainbird-engine": engine }),
  };

  const url = `${baseURL}/start/${kmID}${
    context
      ? `?${encodeURIComponent("contextId")}=${encodeURIComponent(context)}`
      : ""
  }`;

  const response = await fetch(url, { headers });
  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  const data = await response.json();
  return { sessionID: data.id, kmVersionID: data.kmVersion.id };
};
