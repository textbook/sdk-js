import { OptionsEvidence, EvidenceResponse } from "./domain";

// Returns question(s) or result
export async function evidence(
  baseURL: string,
  sessionID: string,
  factID: string,
  options: OptionsEvidence = {
    engine: undefined,
    evidenceKey: undefined,
  },
): Promise<EvidenceResponse> {
  const { fetch } = global;
  const { engine, evidenceKey } = options;

  const headers = {
    ...(engine && { "x-rainbird-engine": engine }),
    ...(evidenceKey && { "x-evidence-key": evidenceKey }),
  };

  const response = await fetch(
    `${baseURL}/analysis/evidence/${factID}/${sessionID}`,
    {
      method: "GET",
      headers,
    },
  );
  if (!response.ok) throw new Error(`API error code: ${response.status}`);
  return response.json();
}
