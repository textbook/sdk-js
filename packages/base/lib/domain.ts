export type ConceptInstance = string | boolean | number;

type ConceptType = "number" | "string" | "boolean" | "date";

export interface Options {
  engine?: string;
  context?: string;
}

export interface OptionsEvidence {
  engine?: string;
  evidenceKey?: string;
}

export interface Fact {
  subject: ConceptInstance;
  relationship: string;
  object: ConceptInstance;
  certainty?: number; // default at 100 if not specified //
}

interface Metadata {
  [language: string]: {
    dataType: string;
    data: string;
  }[];
}

interface Result {
  subject: ConceptInstance;
  relationshipType: string;
  object: ConceptInstance;
  certainty: number;
  factID: string;
  relationship: string;
  subjectMetadata: Metadata;
  objectMetadata: Metadata;
}

export interface ResultResponse {
  type: symbol; // added by parsing
  data: Result[];
}

interface KnownAnswer {
  subject: ConceptInstance;
  relationship: {
    name: string;
    subject: ConceptInstance;
    object: ConceptInstance;
    plural: boolean;
    allowCertainty: boolean;
    allowUnknown: boolean;
    canAdd: string;
    askable: string;
    // ToDo type questions properly
    questions: any;
    metadata: Metadata;
    fsid: number;
    allowCF: boolean;
    // ToDo type subjectDatasources properly
    subjectDatasources: any[];
    objectType: ConceptType;
    subjectType: ConceptType;
  };
  object: ConceptInstance;
  cf: number;
}

interface ConceptsList {
  conceptType: string;
  name: ConceptInstance;
  type: string;
  value: ConceptInstance;
}

interface Question {
  subject: ConceptInstance;
  dataType: string;
  relationship: string;
  type: string;
  plural: boolean;
  allowCF: boolean;
  allowUnknown: boolean;
  canAdd: boolean;
  prompt: string;
  knownAnswers: KnownAnswer[];
  concepts: ConceptsList[];
}

export interface QuestionResponse {
  type: symbol; // added by parsing
  data: {
    question: Question[];
    // ToDo type extraQuestions properly
    extraQuestions: any[];
  };
}

export interface ConditionRelationship {
  certainty: number;
  factID: string;
  factKey: number;
  object: ConceptInstance;
  objectType: ConceptType;
  relationship: string;
  salience: number;
  subject: ConceptInstance;
}

export interface ConditionExpression {
  wasMet: boolean;
  salience: number;
  expression: { text: string };
}

export interface EvidenceResponse {
  fact: {
    factID: string;
    certainty: number;
    object: {
      type: string;
      value: ConceptInstance;
      dataType: ConceptType;
    };
    relationship: {
      type: string;
    };
    subject: {
      type: string;
      value: ConceptInstance;
      dataType: ConceptType;
    };
  };
  rule: {
    bindings: {
      [variable: string]: ConceptInstance;
    };
    conditions: [ConditionRelationship | ConditionExpression];
  };
  source: string;
  time: number;
}
