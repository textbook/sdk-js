import "core-js/stable";
import "regenerator-runtime/runtime";

import { fullEvidence } from "../full_evidence";

afterEach(() => {
  global.fetch.mockClear();
});

const evidenceFacts = [
  {
    factID:
      "WA:RF:823f022e14e5c7c80210045447c35d8245a2f7d1af7270b69437c2b6df23caf8",
    source: "rule",
    fact: {
      subject: {
        type: "Person",
        value: "Christiaan",
        dataType: "string",
      },
      relationship: {
        type: "should visit overall",
      },
      object: {
        type: "Country",
        value: "Italy",
        dataType: "string",
      },
      certainty: 100,
    },
    time: 1642667872546,
    rule: {
      bindings: {
        S: "Christiaan",
        O: "Italy",
      },
      conditions: [
        {
          subject: "Christiaan",
          relationship: "should visit country based on holiday type",
          object: "Italy",
          salience: 100,
          certainty: 100,
          factID:
            "WA:RF:1237d2d70c1ca2febfdf8f8d4e0a6e0e152c750dedd275d39cf956fe69200026",
          objectType: "string",
          factKey: "57070d07-f47a-4ef2-8d6f-337eee4f055d",
        },
        {
          subject: "Christiaan",
          relationship: "should visit country based on holiday duration",
          object: "Italy",
          salience: 50,
          certainty: 100,
          factID:
            "WA:RF:593a700788ba8274d294ddfbe88c9debc5561fc4fc354601bea05736535135da",
          objectType: "string",
          factKey: "b409b3ed-d5fa-4f46-9311-14f804de40e6",
        },
        {
          subject: "Christiaan",
          relationship: "can afford to go to country",
          object: "Italy",
          salience: 100,
          certainty: 100,
          factID:
            "WA:RF:b20f4d219143dd88a70c330de7f38effe13e7209a539468b33ab6812e8960ef2",
          objectType: "string",
          factKey: "1b5dbd73-e2fd-4753-b152-02d95f5c8098",
        },
      ],
    },
  },
  {
    factID:
      "WA:RF:593a700788ba8274d294ddfbe88c9debc5561fc4fc354601bea05736535135da",
    source: "rule",
    fact: {
      subject: {
        type: "Person",
        value: "Christiaan",
        dataType: "string",
      },
      relationship: {
        type: "should visit country based on holiday duration",
      },
      object: {
        type: "Country",
        value: "Italy",
        dataType: "string",
      },
      certainty: 100,
    },
    time: 1642667872473,
    rule: {
      bindings: {
        S: "Christiaan",
        O: "Italy",
        HOLIDAY_DURATION: "2 weeks",
      },
      conditions: [
        {
          subject: "Christiaan",
          relationship: "would like holiday duration",
          object: "2 weeks",
          salience: 50,
          certainty: 100,
          factID:
            "WA:AF:a147f6b3f022e8792128b09f331ed2eca3bbbef69f059758e0983deda9844071",
          objectType: "string",
          factKey: "9c6ed3f9-c61f-458c-96fe-6262ccc518d2",
        },
        {
          subject: "2 weeks",
          relationship: "suits country",
          object: "Italy",
          salience: 24,
          certainty: 100,
          factID:
            "WA:AF:70ca30173bfd70ed957de4f3eb4eaafa95eca01a568a7ed4c1583996a741bea3",
          objectType: "string",
          factKey: "6302d7fe-fdfe-42ee-b715-1550ab2f8690",
        },
      ],
    },
  },
  {
    factID:
      "WA:RF:b20f4d219143dd88a70c330de7f38effe13e7209a539468b33ab6812e8960ef2",
    source: "rule",
    fact: {
      subject: {
        type: "Person",
        value: "Christiaan",
        dataType: "string",
      },
      relationship: {
        type: "can afford to go to country",
      },
      object: {
        type: "Country",
        value: "Italy",
        dataType: "string",
      },
      certainty: 100,
    },
    time: 1642667872546,
    rule: {
      bindings: {
        S: "Christiaan",
        O: "Italy",
        BUDGET: 3000,
        PRICE: 450,
      },
      conditions: [
        {
          subject: "Christiaan",
          relationship: "has a budget",
          object: 3000,
          salience: 100,
          certainty: 100,
          factID:
            "WA:AF:4cc25597a888436f655596e79c0fed7638a64c905571c661c2d8489226ab8c6f",
          objectType: "number",
          factKey: "3050486f-d1fc-495b-8ddc-e650eea59623",
        },
        {
          subject: "Italy",
          relationship: "country price to visit",
          object: 450,
          salience: 100,
          certainty: 100,
          factID:
            "WA:KF:8e26f4a26305f28348d121e0ef05d0b4782a327b719e8ad0d6373b21c256e81c",
          objectType: "number",
        },
        {
          wasMet: true,
          salience: 100,
          expression: {
            text: "%PRICE lte %BUDGET",
          },
        },
      ],
    },
  },
  {
    factID:
      "WA:RF:1237d2d70c1ca2febfdf8f8d4e0a6e0e152c750dedd275d39cf956fe69200026",
    source: "rule",
    fact: {
      subject: {
        type: "Person",
        value: "Christiaan",
        dataType: "string",
      },
      relationship: {
        type: "should visit country based on holiday type",
      },
      object: {
        type: "Country",
        value: "Italy",
        dataType: "string",
      },
      certainty: 100,
    },
    time: 1642667872317,
    rule: {
      bindings: {
        S: "Christiaan",
        O: "Italy",
        HOLIDAY_TYPE: "Skiing",
      },
      conditions: [
        {
          subject: "Christiaan",
          relationship: "would like holiday type",
          object: "Skiing",
          salience: 25,
          certainty: 100,
          factID:
            "WA:AF:1b66fb71aa321017ceaea341378017e6a6b0f663a33cc5dc5f289f0956395d0c",
          objectType: "string",
          factKey: "b7a3882a-f68b-4b4d-9202-bd90c50ee298",
        },
        {
          subject: "Skiing",
          relationship: "is available in country",
          object: "Italy",
          salience: 25,
          certainty: 100,
          factID:
            "WA:KF:eebc2e3d5b355e59d2f9e888ba67ddb9dbab653181aa9aa545932e4d0db9165a",
          objectType: "string",
        },
      ],
    },
  },
];

const normalResp = [
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "should visit overall" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:823f022e14e5c7c80210045447c35d8245a2f7d1af7270b69437c2b6df23caf8",
    rule: {
      bindings: { O: "Italy", S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:RF:1237d2d70c1ca2febfdf8f8d4e0a6e0e152c750dedd275d39cf956fe69200026",
          factKey: "57070d07-f47a-4ef2-8d6f-337eee4f055d",
          object: "Italy",
          objectType: "string",
          relationship: "should visit country based on holiday type",
          salience: 100,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:RF:593a700788ba8274d294ddfbe88c9debc5561fc4fc354601bea05736535135da",
          factKey: "b409b3ed-d5fa-4f46-9311-14f804de40e6",
          object: "Italy",
          objectType: "string",
          relationship: "should visit country based on holiday duration",
          salience: 50,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:RF:b20f4d219143dd88a70c330de7f38effe13e7209a539468b33ab6812e8960ef2",
          factKey: "1b5dbd73-e2fd-4753-b152-02d95f5c8098",
          object: "Italy",
          objectType: "string",
          relationship: "can afford to go to country",
          salience: 100,
          subject: "Christiaan",
        },
      ],
    },
    source: "rule",
    time: 1642667872546,
  },
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "should visit country based on holiday duration" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:593a700788ba8274d294ddfbe88c9debc5561fc4fc354601bea05736535135da",
    rule: {
      bindings: { HOLIDAY_DURATION: "2 weeks", O: "Italy", S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:AF:a147f6b3f022e8792128b09f331ed2eca3bbbef69f059758e0983deda9844071",
          factKey: "9c6ed3f9-c61f-458c-96fe-6262ccc518d2",
          object: "2 weeks",
          objectType: "string",
          relationship: "would like holiday duration",
          salience: 50,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:AF:70ca30173bfd70ed957de4f3eb4eaafa95eca01a568a7ed4c1583996a741bea3",
          factKey: "6302d7fe-fdfe-42ee-b715-1550ab2f8690",
          object: "Italy",
          objectType: "string",
          relationship: "suits country",
          salience: 24,
          subject: "2 weeks",
        },
      ],
    },
    source: "rule",
    time: 1642667872473,
  },
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "can afford to go to country" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:b20f4d219143dd88a70c330de7f38effe13e7209a539468b33ab6812e8960ef2",
    rule: {
      bindings: { BUDGET: 3000, O: "Italy", PRICE: 450, S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:AF:4cc25597a888436f655596e79c0fed7638a64c905571c661c2d8489226ab8c6f",
          factKey: "3050486f-d1fc-495b-8ddc-e650eea59623",
          object: 3000,
          objectType: "number",
          relationship: "has a budget",
          salience: 100,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:KF:8e26f4a26305f28348d121e0ef05d0b4782a327b719e8ad0d6373b21c256e81c",
          object: 450,
          objectType: "number",
          relationship: "country price to visit",
          salience: 100,
          subject: "Italy",
        },
        {
          expression: { text: "%PRICE lte %BUDGET" },
          salience: 100,
          wasMet: true,
        },
      ],
    },
    source: "rule",
    time: 1642667872546,
  },
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "should visit country based on holiday type" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:1237d2d70c1ca2febfdf8f8d4e0a6e0e152c750dedd275d39cf956fe69200026",
    rule: {
      bindings: { HOLIDAY_TYPE: "Skiing", O: "Italy", S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:AF:1b66fb71aa321017ceaea341378017e6a6b0f663a33cc5dc5f289f0956395d0c",
          factKey: "b7a3882a-f68b-4b4d-9202-bd90c50ee298",
          object: "Skiing",
          objectType: "string",
          relationship: "would like holiday type",
          salience: 25,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:KF:eebc2e3d5b355e59d2f9e888ba67ddb9dbab653181aa9aa545932e4d0db9165a",
          object: "Italy",
          objectType: "string",
          relationship: "is available in country",
          salience: 25,
          subject: "Skiing",
        },
      ],
    },
    source: "rule",
    time: 1642667872317,
  },
];

const noRule = {
  factID:
    "WA:AF:899b29c8e81ef65820aa4353b0cc180ac842a12eef1ec0518071ec9e83e72672",
  source: "answer",
  fact: {
    subject: {
      type: "Person",
      value: "Christiaan",
      dataType: "string",
    },
    relationship: {
      type: "would like holiday type",
    },
    object: {
      type: "Holiday type",
      value: "Skiing",
      dataType: "string",
    },
    certainty: 100,
  },
  time: 1642687289893,
};

const noRuleIncluded = [
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "should visit overall" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:823f022e14e5c7c80210045447c35d8245a2f7d1af7270b69437c2b6df23caf8",
    rule: {
      bindings: { O: "Italy", S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:RF:1237d2d70c1ca2febfdf8f8d4e0a6e0e152c750dedd275d39cf956fe69200026",
          factKey: "57070d07-f47a-4ef2-8d6f-337eee4f055d",
          object: "Italy",
          objectType: "string",
          relationship: "should visit country based on holiday type",
          salience: 100,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:RF:593a700788ba8274d294ddfbe88c9debc5561fc4fc354601bea05736535135da",
          factKey: "b409b3ed-d5fa-4f46-9311-14f804de40e6",
          object: "Italy",
          objectType: "string",
          relationship: "should visit country based on holiday duration",
          salience: 50,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:RF:b20f4d219143dd88a70c330de7f38effe13e7209a539468b33ab6812e8960ef2",
          factKey: "1b5dbd73-e2fd-4753-b152-02d95f5c8098",
          object: "Italy",
          objectType: "string",
          relationship: "can afford to go to country",
          salience: 100,
          subject: "Christiaan",
        },
      ],
    },
    source: "rule",
    time: 1642667872546,
  },
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "should visit country based on holiday duration" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:593a700788ba8274d294ddfbe88c9debc5561fc4fc354601bea05736535135da",
    rule: {
      bindings: { HOLIDAY_DURATION: "2 weeks", O: "Italy", S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:AF:a147f6b3f022e8792128b09f331ed2eca3bbbef69f059758e0983deda9844071",
          factKey: "9c6ed3f9-c61f-458c-96fe-6262ccc518d2",
          object: "2 weeks",
          objectType: "string",
          relationship: "would like holiday duration",
          salience: 50,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:AF:70ca30173bfd70ed957de4f3eb4eaafa95eca01a568a7ed4c1583996a741bea3",
          factKey: "6302d7fe-fdfe-42ee-b715-1550ab2f8690",
          object: "Italy",
          objectType: "string",
          relationship: "suits country",
          salience: 24,
          subject: "2 weeks",
        },
      ],
    },
    source: "rule",
    time: 1642667872473,
  },
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "can afford to go to country" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:b20f4d219143dd88a70c330de7f38effe13e7209a539468b33ab6812e8960ef2",
    rule: {
      bindings: { BUDGET: 3000, O: "Italy", PRICE: 450, S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:AF:4cc25597a888436f655596e79c0fed7638a64c905571c661c2d8489226ab8c6f",
          factKey: "3050486f-d1fc-495b-8ddc-e650eea59623",
          object: 3000,
          objectType: "number",
          relationship: "has a budget",
          salience: 100,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:KF:8e26f4a26305f28348d121e0ef05d0b4782a327b719e8ad0d6373b21c256e81c",
          object: 450,
          objectType: "number",
          relationship: "country price to visit",
          salience: 100,
          subject: "Italy",
        },
        {
          expression: { text: "%PRICE lte %BUDGET" },
          salience: 100,
          wasMet: true,
        },
      ],
    },
    source: "rule",
    time: 1642667872546,
  },
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Country", value: "Italy" },
      relationship: { type: "should visit country based on holiday type" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:RF:1237d2d70c1ca2febfdf8f8d4e0a6e0e152c750dedd275d39cf956fe69200026",
    rule: {
      bindings: { HOLIDAY_TYPE: "Skiing", O: "Italy", S: "Christiaan" },
      conditions: [
        {
          certainty: 100,
          factID:
            "WA:AF:1b66fb71aa321017ceaea341378017e6a6b0f663a33cc5dc5f289f0956395d0c",
          factKey: "b7a3882a-f68b-4b4d-9202-bd90c50ee298",
          object: "Skiing",
          objectType: "string",
          relationship: "would like holiday type",
          salience: 25,
          subject: "Christiaan",
        },
        {
          certainty: 100,
          factID:
            "WA:KF:eebc2e3d5b355e59d2f9e888ba67ddb9dbab653181aa9aa545932e4d0db9165a",
          object: "Italy",
          objectType: "string",
          relationship: "is available in country",
          salience: 25,
          subject: "Skiing",
        },
      ],
    },
    source: "rule",
    time: 1642667872317,
  },
  {
    fact: {
      certainty: 100,
      object: { dataType: "string", type: "Holiday type", value: "Skiing" },
      relationship: { type: "would like holiday type" },
      subject: { dataType: "string", type: "Person", value: "Christiaan" },
    },
    factID:
      "WA:AF:899b29c8e81ef65820aa4353b0cc180ac842a12eef1ec0518071ec9e83e72672",
    source: "answer",
    time: 1642687289893,
  },
];

describe("fullEvidence", () => {
  it("should perform a simple fullEvidence retrieval", async () => {
    let calls = 0;
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => {
        calls += 1;
        return evidenceFacts[calls - 1];
      },
    }));

    const result = await fullEvidence(
      "thisistheurl",
      "thisisthesession",
      "thisisthefact",
    );
    expect(global.fetch).toHaveBeenCalledTimes(10);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["headers", "method"]);
    expect(call.method).toEqual("GET");
    expect(call.headers).toMatchObject({});
    expect(result).toEqual(normalResp);
  });
  it("should include fact that does not have a rule", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => {
        return noRule;
      },
    }));

    const result = await fullEvidence(
      "thisistheurl",
      "thisisthesession",
      "thisisthefact",
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["headers", "method"]);
    expect(call.method).toEqual("GET");
    expect(call.headers).toMatchObject({});
    expect(result).toEqual(noRuleIncluded);
  });
  it("should throw fetch-generated errors", async () => {
    global.fetch = jest.fn(async () => {
      throw new Error("This was generated by fetch");
    });

    await expect(async () =>
      fullEvidence("thisistheurl", "thisistheid", {}),
    ).rejects.toHaveProperty("message", "This was generated by fetch");
  });
  it("should handle 404", async () => {
    global.fetch = jest.fn(async () => ({
      ok: false,
      status: 404,
      json: async () => ({}),
    }));

    await expect(async () =>
      fullEvidence("thisistheurl", "thisistheid", {}),
    ).rejects.toHaveProperty("message", "API error code: 404");
  });
});
