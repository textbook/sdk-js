import "core-js/stable";
import "regenerator-runtime/runtime";

import { response } from "../response";
import {
  RESPONSE_TYPE_QUESTION,
  RESPONSE_TYPE_RESULT,
} from "../response_types";

afterEach(() => {
  global.fetch.mockClear();
});

describe("response", () => {
  it("should make a response call containing a single answer", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await response("thisistheurl", "thisisthesessionid", [
      {
        subject: "Bob",
        relationship: "lives in",
        object: "England",
      },
    ]);
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      answers: [
        {
          subject: "Bob",
          relationship: "lives in",
          object: "England",
        },
      ],
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should handle a question response", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        question: { foo: "bar" },
      }),
    }));

    const result = await response("thisistheurl", "thisisthesessionid", [
      {
        subject: "Bob",
        relationship: "lives in",
        object: "England",
      },
    ]);
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      answers: [
        {
          subject: "Bob",
          relationship: "lives in",
          object: "England",
        },
      ],
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_QUESTION,
      data: { question: { foo: "bar" }, extraQuestions: [] },
    });
  });

  it("should pass certainty when specified", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await response("thisistheurl", "thisisthesessionid", [
      {
        subject: "Bob",
        relationship: "lives in",
        object: "England",
        certainty: 80,
      },
    ]);
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      answers: [
        {
          subject: "Bob",
          relationship: "lives in",
          object: "England",
          certainty: 80,
        },
      ],
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  describe("should pass answer when specified", () => {
    it("should pass yes as an answer", async () => {
      global.fetch = jest.fn(async () => ({
        json: async () => ({ result: { foo: "bar" } }),
        ok: true,
        status: 200,
      }));

      const payload = {
        answer: "yes",
        certainty: 80,
        object: "England",
        relationship: "lives in",
        subject: "Bob",
      };
      const result = await response("url", "sessionID", [payload]);

      expect(global.fetch).toHaveBeenCalledTimes(1);

      const call = global.fetch.mock.calls[0][1];
      const headers = { "Content-Type": "application/json" };
      const res = { type: RESPONSE_TYPE_RESULT, data: { foo: "bar" } };

      expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
      expect(call.method).toEqual("POST");
      expect(call.headers).toMatchObject(headers);
      expect(JSON.parse(call.body)).toMatchObject({ answers: [payload] });
      expect(result).toEqual(res);
    });

    it("should pass no as an answer", async () => {
      global.fetch = jest.fn(async () => ({
        json: async () => ({ result: { foo: "bar" } }),
        ok: true,
        status: 200,
      }));

      const payload = {
        answer: "no",
        certainty: 80,
        object: "England",
        relationship: "lives in",
        subject: "Bob",
      };
      const result = await response("url", "sessionID", [payload]);

      expect(global.fetch).toHaveBeenCalledTimes(1);

      const call = global.fetch.mock.calls[0][1];
      const headers = { "Content-Type": "application/json" };
      const res = { type: RESPONSE_TYPE_RESULT, data: { foo: "bar" } };

      expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
      expect(call.method).toEqual("POST");
      expect(call.headers).toMatchObject(headers);
      expect(JSON.parse(call.body)).toMatchObject({ answers: [payload] });
      expect(result).toEqual(res);
    });
  });

  it("should make a response call containing multiple answers", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await response("thisistheurl", "thisisthesessionid", [
      {
        subject: "Bob",
        relationship: "lives in",
        object: "England",
      },
      {
        subject: "Bob",
        relationship: "lives in",
        object: "France",
      },
    ]);
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      answers: [
        {
          subject: "Bob",
          relationship: "lives in",
          object: "England",
        },
        {
          subject: "Bob",
          relationship: "lives in",
          object: "France",
        },
      ],
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should make a response call for a specific engine", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        result: { foo: "bar" },
      }),
    }));

    const result = await response(
      "thisistheurl",
      "thisisthesessionid",
      [
        {
          subject: "Bob",
          relationship: "lives in",
          object: "England",
        },
      ],
      { engine: "thisistheengine" },
    );
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({
      "Content-Type": "application/json",
      "x-rainbird-engine": "thisistheengine",
    });
    expect(JSON.parse(call.body)).toMatchObject({
      answers: [
        {
          subject: "Bob",
          relationship: "lives in",
          object: "England",
        },
      ],
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should handle a question response with extra questions for grouped relationships", async () => {
    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => ({
        extraQuestions: [
          { qux: "quux" },
          { quuz: "corge" },
          { grault: "garply" },
        ],
        question: { foo: "bar" },
      }),
    }));

    const result = await response("thisistheurl", "thisisthesessionid", [
      {
        subject: "Bob",
        relationship: "lives in",
        object: "England",
      },
    ]);
    expect(global.fetch).toHaveBeenCalledTimes(1);
    const call = global.fetch.mock.calls[0][1];
    expect(Object.keys(call).sort()).toEqual(["body", "headers", "method"]);
    expect(call.method).toEqual("POST");
    expect(call.headers).toMatchObject({ "Content-Type": "application/json" });
    expect(JSON.parse(call.body)).toMatchObject({
      answers: [
        {
          subject: "Bob",
          relationship: "lives in",
          object: "England",
        },
      ],
    });
    expect(result).toEqual({
      type: RESPONSE_TYPE_QUESTION,
      data: {
        question: { foo: "bar" },
        extraQuestions: [
          { qux: "quux" },
          { quuz: "corge" },
          { grault: "garply" },
        ],
      },
    });
  });

  it("should throw fetch-generated errors", async () => {
    global.fetch = jest.fn(async () => {
      throw new Error("This was generated by fetch");
    });

    await expect(async () =>
      response("thisistheurl", "thisistheid", []),
    ).rejects.toHaveProperty("message", "This was generated by fetch");
  });

  it("should handle 404", async () => {
    global.fetch = jest.fn(async () => ({
      ok: false,
      status: 404,
      json: async () => ({}),
    }));

    await expect(async () =>
      response("thisistheurl", "thisistheid", []),
    ).rejects.toHaveProperty("message", "API error code: 404");
  });

  it("should handle 500", async () => {
    global.fetch = jest.fn(async () => ({
      ok: false,
      status: 500,
      json: async () => ({}),
    }));

    await expect(async () =>
      response("thisistheurl", "thisistheid", []),
    ).rejects.toHaveProperty("message", "API error code: 500");
  });

  it("should handle 400", async () => {
    global.fetch = jest.fn(async () => ({
      ok: false,
      status: 400,
      json: async () => ({}),
    }));

    await expect(async () =>
      response("thisistheurl", "thisistheid", []),
    ).rejects.toHaveProperty("message", "API error code: 400");
  });
});
