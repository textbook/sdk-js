import * as base from "..";

describe("constants", () => {
  const constants = [
    "URL_COMMUNITY",
    "URL_ENTERPRISE",
    "ENGINE_CORE",
    "ENGINE_BETA",
  ];

  constants.forEach((constant) => {
    it(`should export constant ${constant}`, async () => {
      expect(base[constant]).not.toEqual(undefined);
    });
  });
});
