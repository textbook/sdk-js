import "core-js/stable";
import "regenerator-runtime/runtime";

import {
  parse,
  RESPONSE_TYPE_QUESTION,
  RESPONSE_TYPE_RESULT,
  RESPONSE_TYPE_UNKNOWN,
} from "../response_types";

describe("parse", () => {
  it("should handle a question", async () => {
    expect(parse({ question: { foo: "bar" } })).toMatchObject({
      type: RESPONSE_TYPE_QUESTION,
      data: { question: { foo: "bar" } },
    });
  });

  it("should handle extra questions", async () => {
    expect(
      parse({ question: { foo: "bar" }, extraQuestions: [{ qux: "quux" }] }),
    ).toMatchObject({
      type: RESPONSE_TYPE_QUESTION,
      data: { question: { foo: "bar" }, extraQuestions: [{ qux: "quux" }] },
    });
  });

  it("should handle a result", async () => {
    expect(parse({ result: { foo: "bar" } })).toMatchObject({
      type: RESPONSE_TYPE_RESULT,
      data: { foo: "bar" },
    });
  });

  it("should handle a question", async () => {
    expect(parse({ baz: { foo: "bar" } })).toMatchObject({
      type: RESPONSE_TYPE_UNKNOWN,
      data: { baz: { foo: "bar" } },
    });
  });
});
